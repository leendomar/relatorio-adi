<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeedSyncRangeDateLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'newspaper_id',
        'start_date',
        'end_date',
        'status',
        'error_message',
    ];

    public function newspaper()
    {
        return $this->belongsTo(Newspaper::class);
    }
}
