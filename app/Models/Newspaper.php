<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Newspaper extends Model
{
    use HasFactory;

    protected $fillable = [
        'newspaper_id',
        'wp_active',
        'wp_version',
        'tag',
        'url',
        'status',
        'tye_content',
        'name',
        'city',
        'id',
    ];

    public function FeedNewsList(): HasMany
    {
        return $this->hasMany(FeedNewsList::class);
    }

    public function tagUrl($tag = 'govpr')
    {
        return $this->hasMany(NewspaperTagurl::class, 'newspaper_id')->get(['id', 'tag', 'path'])->toArray();
    }
}
