<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeedReader extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'user_id',
        'newspaper_id',
        'tag',
        'count',
        'created_at'
    ];
}
