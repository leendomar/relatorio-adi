<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WppAdiFeedVersion extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'slug', 'version', 'download_url', 'requires', 'tested', 'author', 'homepage', 'upgrade_notice'
    ];
}
