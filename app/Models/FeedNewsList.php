<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FeedNewsList extends Model
{
    use HasFactory;

    protected $fillable = [
        'newspaper_id',
        'view',
        'stay',
        'title',
        'link',
        'date',
        'tag',
        'created_at',
        'updated_at',
    ];

    public function newspapper(): BelongsTo
    {
        return $this->belongsTo(Newspaper::class);
    }
}
