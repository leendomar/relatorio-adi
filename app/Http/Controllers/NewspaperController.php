<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNewspaperRequest;
use App\Http\Requests\UpdateNewspaperRequest;
use App\Models\Newspaper;
use App\Models\Tag;

class NewspaperController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $newspapers = Newspaper::all();

        return view('newspapers.index', compact('newspapers'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('newspapers.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreNewspaperRequest $request)
    {
        $input = $request->all();

        $newspaper = new Newspaper();
        $newspaper->city = $input['city'];
        $newspaper->name = $input['name'];
        $url = rtrim($input['url'], '/');
        $newspaper->url = $url;
        $newspaper->type_content = $input['type_content'];
        $newspaper->save();

        return redirect()->route('newspapers.edit', $newspaper->id)->with('success', 'Jornal salvo com sucesso!');

        // return redirect()->route('newspapers.index')->with('success', 'Jornal salvo com sucesso!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Newspaper $newspaper)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Newspaper $newspaper)
    {
        $newspaper->tagurl = $newspaper->tagUrl();
        $action_icons = [
            'icon:trash | color:red | click:deleteTagurl({id})',
        ];

        $tagsList = Tag::where('is_active', true)->select('client', 'name')->get();

        return view('newspapers.edit', compact('newspaper', 'action_icons', 'tagsList'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateNewspaperRequest $request, Newspaper $newspaper)
    {
        $input = $request->all();

        $newspaper->city = $input['city'];
        $newspaper->name = $input['name'];
        $url = rtrim($input['url'], '/');
        $newspaper->url = $url;
        $newspaper->type_content = $input['type_content'];
        $newspaper->save();

        return redirect()->route('newspapers.edit', $newspaper->id)->with('success', 'Jornal atualizado com sucesso!');

        // return redirect()->route('newspapers.index')->with('success', 'Jornal atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Newspaper $newspaper)
    {
        $newspaper->status = $newspaper->status == 'inativo' ? 'ativo' : 'inativo';
        $newspaper->save();

        return redirect()->route('newspapers.index')->with('success', 'Jornal Excluido com sucesso!');
    }
}
