<?php

namespace App\Http\Controllers;

use App\Models\FeedNewsList;
use App\Models\FeedReader as ModelsFeedReader;
// use App\Models\NewspaperTagurl;
use App\Models\Newspaper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Vedmant\FeedReader\Facades\FeedReader;

class FeedReaderController extends Controller
{
    private $jornais = [];

    public function __construct()
    {
        $this->jornais = Newspaper::where('status', '=', 'ativo')
        ->where('wp_active', false)
        ->get()->toArray();

        foreach ($this->jornais as &$jornal) {
            $jornal['tagUrl'] = Newspaper::find($jornal['id'])->tagUrl();
        }
    }

    public function index(Request $request)
    {
        set_time_limit(0);

        // date_default_timezone_set('America/Sao_Paulo');
        foreach ($this->jornais as $key => $jornal) {
            foreach ($jornal['tagUrl'] as $tag_url) {
                if (isset($tag_url['tag'])) {
                    $countNews = 0;

                    $user_id = Auth::user();

                    $data = [
                        'user_id' => $user_id['id'] ?? 1,
                        'tag' => $tag_url['tag'],
                        'newspaper_id' => $jornal['id'],
                        'created_at' => date('Y-m-d H:i:s'),
                    ];

                    $url = $jornal['url'].$tag_url['path'];

                    $this->getFeedNews($url, $key);

                    if ($this->jornais[$key]['news']) {
                        foreach ($this->jornais[$key]['news'] as $news) {
                            $Newspaper = Newspaper::find($jornal['id']);
                            $n = $Newspaper->FeedNewsList()
                                ->where('title', $news['title'])->select('link')
                                ->first();
                            if (!$n) {
                                $news['newspaper_id'] = $jornal['id'];
                                $news['tag'] = $tag_url['tag'];
                                $date = date('Y-m-d H:i:s');
                                $news['created_at'] = $date;
                                $news['updated_at'] = $date;
                                $feedNews = new FeedNewsList($news);
                                $Newspaper->FeedNewsList()->save($feedNews);
                                ++$countNews;
                            }
                        }
                    }

                    $data['count'] = $countNews;
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    if ((bool) $countNews) {
                        $ModelsFeedReader = new ModelsFeedReader($data);
                        $ModelsFeedReader->save();
                    }
                }
            }
        }
        echo 'ok';
        exit;
    }

    private function getFeedNews($url, $key)
    {
        $this->jornais[$key]['news'] = [];
        $headers = [
            'User-Agent' => 'WPP-ADI-FEED/1',
        ];
        $feeds = FeedReader::read($url, 'default', $headers);

        if ($feeds->data) {
            foreach ($feeds->get_items() as $feed) {
                $this->jornais[$key]['news'][] = [
                    'title' => $feed->get_title(),
                    'link' => $this->removeUrlQuery($feed->get_link()),
                    'date' => date('Y-m-d H:i:s', strtotime($feed->get_date())),
                ];
            }
        }
    }

    private function removeUrlQuery($url)
    {
        $url = parse_url($url);

        return $url['scheme'].'://'.$url['host'].$url['path'];
    }

    private function getFeedData($feed, $field)
    {
        return data_get($feed->data, "child.http://purl.org/dc/elements/1.1/.$field.0.data", null);
    }
}
