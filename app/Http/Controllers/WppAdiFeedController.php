<?php

namespace App\Http\Controllers;

use App\Models\WppAdiFeedVersion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class WppAdiFeedController extends Controller
{
    public function index()
    {
        $defaults = WppAdiFeedVersion::latest()->first();
        if(!$defaults){
            $defaults = [
                'name' => 'WPP ADI Feed',
                'slug' => 'wpp-adi-feed/wpp-adi-feed.php',
                'version' => '1.2.9',
                'requires' => '5.0',
                'tested' => '6.4',
                'author' => 'Trivium Tech - Lindomar',
                'homepage' => 'https://trivium.dev.br',
                'upgrade_notice' => 'Correção de bugs e melhorias de desempenho.',
            ];
        }

        return view('wpp-feed-adi.index', compact('defaults'));
    }

    public function upload(Request $request)
    {
        $request->validate([
            'plugin' => 'required|file|mimes:zip|max:10240', // Limite de 10MB
            'name' => 'required|string',
            'slug' => 'required|string',
            'version' => 'required|string',
            'requires' => 'required|string',
            'tested' => 'required|string',
            'author' => 'required|string',
            'homepage' => 'required|url',
            'upgrade_notice' => 'nullable|string',
        ]);

        // Fazer upload do arquivo
        $path = $request->file('plugin')->storeAs('public/downloads', $request->file('plugin')->getClientOriginalName());

        // Salvar dados na tabela plugin_versions
        $plugin = WppAdiFeedVersion::create([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'version' => $request->input('version'),
            'download_url' => Storage::url($path),
            'requires' => $request->input('requires'),
            'tested' => $request->input('tested'),
            'author' => $request->input('author'),
            'homepage' => $request->input('homepage'),
            'upgrade_notice' => $request->input('upgrade_notice', 'Nenhuma atualização descrita.'),
        ]);

        return redirect()->back()->with('success', 'Plugin enviado e versão salva com sucesso!');
    }

    public function getPluginUpdateFeed()
    {
        $latestPlugin = WppAdiFeedVersion::latest()->first();

        return response()->json([
            'name' => $latestPlugin->name,
            'slug' => $latestPlugin->slug,
            'version' => $latestPlugin->version,
            'download_url' => asset($latestPlugin->download_url),
            'requires' => $latestPlugin->requires,
            'tested' => $latestPlugin->tested,
            'author' => $latestPlugin->author,
            'homepage' => $latestPlugin->homepage,
            'upgrade_notice' => $latestPlugin->upgrade_notice,
        ]);
    }
}
