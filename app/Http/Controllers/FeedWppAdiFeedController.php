<?php

namespace App\Http\Controllers;

use App\Models\Newspaper;
use App\Services\FeedAdiService;
use App\Services\FeedSyncService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FeedWppAdiFeedController extends Controller
{
    protected $FeedAdiService;
    protected $feedSyncService;

    public function __construct(FeedAdiService $FeedAdiService, FeedSyncService $feedSyncService)
    {
        $this->FeedAdiService = $FeedAdiService;
        $this->feedSyncService = $feedSyncService;
    }

    public function index()
    {
        $newspapers = Newspaper::where('status', 'ativo')
            ->where('wp_active', 1)
            ->select('id', 'name', 'url')
            ->get();

        $last24Hours = today()->subDay()->format('Y-m-d');
        $now = today()->format('Y-m-d');

        foreach ($newspapers as $newspaper) {
            // ProcessFeedByDateRangeJob::dispatch($newspaper, $start_date, $end_date, $logId);
            // Workin in Jobs high
            $this->feedSyncService->processNewspaper($newspaper, $last24Hours, $now, 'high');
        }

        return response()->json(['message' => 'Jobs de processamento de feeds enviados com sucesso.']);
    }

    public function feedDirectUrl(Request $request)
    {
        $newspaper_id = $request->query('newspaper_id');
        $newspapers = Newspaper::where('id', $newspaper_id)
            ->where('wp_active', 1)
            ->select('id', 'name', 'url')
            ->get();

        foreach ($newspapers as $newspaper) {
            $this->FeedAdiService->processFeedUrls($newspaper);
        }
    }

    public function wpActiveAllDomain($wp_active = true)
    {
        $newspapers = Newspaper::where('status', 'ativo')
        ->select('url')
        ->get();

        $results = [];
        foreach ($newspapers as $newspaper) {
            $results[] = $this->isPluginInstalled($newspaper->url, $wp_active);
        }

        return response()->json($results);
    }

    public function isPluginInstalled($domain, $wp_active)
    {
        // Valida a URL
        if (!filter_var($domain, FILTER_VALIDATE_URL)) {
            Log::error("URL inválida: $domain");

            return;
        }

        $status = [];
        try {
            // Verifica se a requisição foi bem-sucedida
            $isPluginInstalled = $this->FeedAdiService->isPluginInstalled($domain);

            if ($isPluginInstalled) {
                // Se instalado, chama a função wpActive para ativar o wp_active
                $status = $this->activeDomains($domain, $wp_active);
            } else {
                // Se não instalado, você pode optar por desativar o wp_active
                $status = $this->activeDomains($domain, false);
                // Log::error("Plugin WPP ADI FEED parece não estar ativo em {$domain}");
            }
        } catch (\Exception $e) {
            // Log do erro
            Log::error("Erro ao acessar a URL: $url - ".$e->getMessage());

            // Você pode optar por desativar o wp_active em caso de erro
            // $status = $this->activeDomains($domain, false);
        }

        return $status;
    }

    public function wpActive(Request $request)
    {
        return response()->json($this->activeDomains($request->domain, $request->wp_active));
    }

    private function activeDomains($domain, $wp_active)
    {
        $status = [];

        $newspaper = Newspaper::where('url', $domain)
            ->select('id', 'name', 'wp_active')
            ->first();

        if ($newspaper) {
            try {
                $newspaper->update([
                    'wp_active' => $wp_active,
                    'wp_version' => $this->FeedAdiService->wp_version,
                ]);

                $status = [
                    'message' => [
                        'Perfil atualizado com sucesso no sistema de relatórios ADI: ',
                        $newspaper->wp_active,
                        $domain,
                    ],
                ];
            } catch (\Exception $e) {
                $status = ['error' => $e->getMessage()];
            }
        } else {
            $status = [
                'message' => [
                    'Perfil deste portal não foi encontrado.',
                    $domain,
                ],
            ];
        }

        return $status;
    }
}
