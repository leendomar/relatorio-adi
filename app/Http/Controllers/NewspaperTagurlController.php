<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewspaperTagurlRequest;
use Request;
use App\Models\NewspaperTagurl;


class NewspaperTagurlController extends Controller
{

    public function store(NewspaperTagurlRequest $request)
    {


        $NewspaperTagurl = new NewspaperTagurl();

        $response = [
            'success' => false,
            'data' => $request->all()
        ];
        $exist = $NewspaperTagurl->where([
            'newspaper_id' => $request->newspaper_id,
            'tag' => $request->tag
        ])->get()->toArray();

        if (!$exist) {
            $NewspaperTagurl['newspaper_id'] = $request->newspaper_id;
            $NewspaperTagurl['tag'] = $request->tag;
            $NewspaperTagurl['path'] = $request->path;
            $response['success'] = $NewspaperTagurl->save();
            $response['data'] = $NewspaperTagurl->where([
                'newspaper_id' => $request->newspaper_id,
                'tag' => $request->tag
            ])->get()->toArray()[0];

        } else {
            $response['error'] = 'TAG já está registrada para este jornal.';
        }
        
        return response()->json( $response );
    }

    public function destroy(NewspaperTagurl $newspaperTagurl){

        $response = [
            'success' =>  $newspaperTagurl->delete(),
            'data' => $newspaperTagurl
        ];

        return response()->json( $response );

    }

}
