<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\UpdateNewsStatsJob;
use Illuminate\Http\Request;

class AnalyticsController extends Controller
{
    public function updateNewsStats(Request $request)
    {
        $validated = $request->validate([
            'url' => 'required|url',
            'time_spent' => 'required|min:0',
        ]);

        dispatch(new UpdateNewsStatsJob($validated['url'], $validated['time_spent']))->onQueue('default');

        return response()->json(['message' => 'Dados recebidos', 'status' => 'success']);
    }

    public function checkScript($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            return response()->json(['error' => 'URL inválida'], 400);
        }

        try {
            // Faz um GET na home do site
            $response = Http::get($url);

            // Verifica se o HTML contém o script
            $scriptIdentifier = 'feed-adi-analytics.js'; // Altere para o nome real do script
            $isInstalled = str_contains($response->body(), $scriptIdentifier);

            return response()->json([
                'url' => $url,
                'script_installed' => $isInstalled,
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Falha ao acessar o site'], 500);
        }
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
    }
}
