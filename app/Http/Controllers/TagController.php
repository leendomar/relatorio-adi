<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    // Exibir a lista de tags
    public function index()
    {
        $tags = Tag::all();

        return view('tags.index', compact('tags'));
    }

    // Exibir o formulário de criação de tag
    public function create()
    {
        return view('tags.create');
    }

    // Exibir o formulário de edição de tag
    public function edit($id)
    {
        $tag = Tag::findOrFail($id);

        return view('tags.edit', compact('tag'));
    }

    public function show()
    {
    }

    // Salvar uma nova tag
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|unique:tags,name|max:255',
        ]);

        Tag::create($request->all());

        return redirect()->route('tags.index');
    }

    // Atualizar uma tag existente
    public function update(Request $request, $id)
    {
        $tag = Tag::findOrFail($id);

        $request->validate([
            'name' => 'required|string|unique:tags,name,'.$tag->id.'|max:255',
        ]);

        $tag->update($request->all());

        return redirect()->route('tags.index');
    }

    // Deletar uma tag
    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);

        // Alterna o estado de ativo/inativo
        $tag->is_active = !$tag->is_active;
        $tag->save();

        $status = $tag->is_active ? 'ativada' : 'desativada';

        return redirect()->route('tags.index')->with('success', "Tag {$status} com sucesso.");
    }

    public function list()
    {
        return Tag::where('is_active', true)->select('client', 'name')->get();
    }
}
