<?php

namespace App\Http\Controllers;

use App\Exports\FeedNewsListExport;
use App\Http\Requests\UpdateFeedNewsListRequest;
use App\Models\FeedNewsList;
use App\Models\FeedReader;
use App\Models\Newspaper;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Yajra\DataTables\Facades\DataTables;

class FeedNewsListController extends Controller
{
    private $feedJornais = [];

    private $flash_data = [];

    private $clientesList = [
        ['label' => 'Governo Federal', 'value' => 'Governo Federal'],
        ['label' => 'Governo do Estado', 'value' => 'Governo do Estado'],
        ['label' => 'Assembleia Legislativa', 'value' => 'Assembleia Legislativa'],
        ['label' => 'Itaipu Binacional', 'value' => 'Itaipu Binacional'],
    ];

    public function __construct()
    {
        $this->feedJornais = Newspaper::where('status', '=', 'ativo')->get()->toArray();
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (!Session::get('Filter.feed.date_after')) {
            Session::put('Filter.feed.date_after', date('Y-m-01'));
        }
        if (!Session::get('Filter.feed.date_before')) {
            Session::put('Filter.feed.date_before', date('Y-m-d'));
        }
        if (!Session::get('Filter.feed.journal_list')) {
            Session::put('Filter.feed.journal_list', $this->journalIds());
        }
        if (!Session::get('Filter.feed.journal_ids')) {
            Session::put('Filter.feed.journal_ids', $this->journalIds(true));
        }

        if (\request()->ajax()) {
            $data = FeedNewsList::latest()->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '';

                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        // dd(Newspaper::where('status', 'ativo')->select('id', 'name')->get())->toArray();

        return view(
            'feed-news-list/index',
            [
                'tagsList' => Tag::where('is_active', true)->select('client', 'name')->get()->toArray(),
                'journalList' => Session::get('Filter.feed.journal_list'),
                'feedJornais' => Session::get('feedJornais'),
                'tag' => Session::get('Filter.feed.tag'),
                'clientes' => $this->clientesList,
                'lestFeedReader' => FeedReader::all()->last()->toArray(),
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'tag' => 'required',
        ]);

        // limpar sessão
        $this->clearFilter();
        $tag = strtolower($request->all()['tag']);
        $title_terms = $request->title_terms;
        $date_after = $request->date_after;
        $date_before = $request->date_before;
        $journalIds = $request->journal_ids;
        $showJournals = $request->showJournals;
        Session::put('Filter.feed.tag', $tag);
        Session::put('Filter.feed.title_terms', $title_terms);
        Session::put('Filter.feed.showJournals', $showJournals);

        Session::put('Filter.feed.journal_ids', $journalIds);

        if (!Session::get('Filter.feed.journal_ids')) {
            Session::put('Filter.feed.journal_ids', $this->journalIds(true));
            $journalIds = $this->journalIds(true);
        }

        if (!$date_after) {
            $date_after = date('Y-m-01');
        }

        if (!$date_before) {
            $date_before = date('Y-m-d');
        }

        Session::put('Filter.feed.date_after', $date_after);
        Session::put('Filter.feed.date_before', $date_before);

        foreach ($this->feedJornais as &$jornal) {
            $jornal['news'] = Newspaper::find($jornal['id'])
                ->FeedNewsList()
                ->where('tag', $tag)
                ->where('title', 'LIKE', "%{$title_terms}%")
                ->where('date', '>=', $date_after.' 00:00:00')
                ->where('date', '<=', $date_before.' 23:59:59')
                ->whereIn('newspaper_id', $journalIds)
                ->orderBy('date', 'desc')
                ->get()
                ->toArray();
            if ((bool) $jornal['news']) {
                Session::put('haveFeedNews', true);
            }
        }

        Session::put('feedJornais', $this->feedJornais);

        Session::flash('flash_data', $this->flash_data);

        Log::info('feedNews', $this->flash_data);

        return view(
            'feed-news-list/index',
            [
                'tagsList' => Tag::where('is_active', true)->select('client', 'name')->get(),
                'jornalIds' => $journalIds,
                'journalList' => Session::get('Filter.feed.journal_list'),
                'feedJornais' => Session::get('feedJornais'),
                'tag' => Session::get('Filter.feed.tag'),
                'lestFeedReader' => FeedReader::all()->last()->toArray(),
                'clientes' => $this->clientesList,
            ]
        );
    }

    /**
     * Display the specified resource.
     */
    public function show(FeedNewsList $feedNewsList)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FeedNewsList $feedNewsList)
    {
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFeedNewsListRequest $request, FeedNewsList $feedNewsList)
    {
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(FeedNewsList $feedNewsList)
    {
    }

    // Generate PDF
    public function createPDF(Request $request)
    {
        // dd($request->view_stay);
        // Dados que você irá passar para a view
        $data = [
            'feedJornais' => Session::get('feedJornais'),
            'tag' => Session::get('Filter.feed.tag'),
            'obs' => $request->observacao,
            'cliente' => $request->cliente,
            'view' => $request->view,
            'stay' => $request->stay,
            'date_after' => date('d-m-Y', strtotime(Session::get('Filter.feed.date_after'))),
            'date_before' => date('d-m-Y', strtotime(Session::get('Filter.feed.date_before'))),
        ];

        // Compartilhar os dados com a view
        view()->share('NewsList', $data);

        // Gerar o PDF com a view 'relatorio/feed-news-list-pdf' e passar os dados
        $pdf = \PDF::loadView('relatorio/feed-news-list-pdf', $data);
        // Definir o tamanho do papel e a orientação
        $pdf->setPaper('A4', 'landscape');

        // Gerar e retornar o PDF.
        // A função 'stream()' serve para exibir o PDF no navegador.
        return $pdf->stream('noticias-por-tag.pdf');
    }

    public function clearFilter()
    {
        Session::put('haveFeedNews', false);
        Session::put('Filter.feed.tag', '');
        Session::put('Filter.feed.date_after', '');
        Session::put('Filter.feed.date_before', '');
        Session::put('feedJornais', []);

        Session::put('Filter.feed.title_terms', '');

        Session::put('Filter.feed.journal_list', $this->journalIds());
        Session::put('Filter.feed.journal_ids', $this->journalIds(true));

        Session::put('Filter.feed.showJournals', '');

        return redirect()->route('feed-news-list.index');
    }

    private function journalIds($ids = false)
    {
        $journals = Newspaper::where('status', 'ativo')->select('id', 'name')->get()->toArray();
        if ($ids) {
            foreach ($journals as $key => &$journal) {
                $journal = $journal['id'];
            }
        }

        return $journals;
    }

    /**
     * Exportar os dados para Excel.
     */
    public function exportExcel(Request $request)
    {
        // Dados a serem passados para o export
        $feedJornais = session('feedJornais');

        $tag = session('Filter.feed.tag');
        $date_after = date('d-m-Y', strtotime(session('Filter.feed.date_after')));
        $date_before = date('d-m-Y', strtotime(session('Filter.feed.date_before')));
        $cliente = $request->cliente;
        $obs = $request->observacao;
        $view = $request->view;
        $stay = $request->stay;

        // Realiza a exportação com os dados
        return Excel::download(
            // new FeedNewsListExport($feedJornais, $tag, $date_after, $date_before, $cliente, $obs, $view, $stay),
            new FeedNewsListExport($feedJornais,  $view, $stay),
            'noticias-por-tag-'.$tag.'.xlsx'
        );
    }
}
