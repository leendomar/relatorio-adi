<?php

namespace App\Http\Controllers;

use App\Exports\NewsListExport;
use App\Models\FeedNewsList;
use App\Models\FeedReader as ModelsFeedReader;
use App\Models\Newspaper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class NewsListController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    private $jornais = [];
    private $flash_data = [];
    private $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3';

    public function __construct()
    {
        $this->jornais = Newspaper::where('status', '=', 'ativo')->get()->toArray();
    }

    public function index()
    {
        if (!Session::get('Filter.date_after')) {
            Session::put('Filter.date_after', date('Y-m-01'));
        }
        if (!Session::get('Filter.date_before')) {
            Session::put('Filter.date_before', date('Y-m-d'));
        }

        $jornais = Session::get('jornais');

        return view('news-list', [
            'jornais' => $jornais,
            'tag' => '',
        ]);
    }

    public function preview()
    {
        $data = [
            'jornais' => Session::get('jornais'),
            'tag' => Session::get('Filter.tag'),
            'date_after' => date('d-m-Y', strtotime(Session::get('Filter.date_after'))),
            'date_before' => date('d-m-Y', strtotime(Session::get('Filter.date_before'))),
        ];
        dd($data);

        $newsData = \App\Models\News::all()->groupBy('journal_name')->map(function ($data) {
            return $news->map(function ($item) {
                return [
                    'title' => 'x',
                    'link' => 'x',
                    'date' => 'x',
                ];
            });
        })->toArray();

        return view('news_preview', compact('newsData'));
    }

    // Generate PDF
    public function createPDF()
    {
        $data = [
            'jornais' => Session::get('jornais'),
            'tag' => Session::get('Filter.tag'),
            'date_after' => date('d-m-Y', strtotime(Session::get('Filter.date_after'))),
            'date_before' => date('d-m-Y', strtotime(Session::get('Filter.date_before'))),
        ];

        view()->share('NewsList', $data);
        $pdf = \PDF::loadView('relatorio/news-list-pdf', $data);
        $pdf->setPaper('A4', 'landscape');
        $pdf->settlabel = 'pdfdd';
        // download PDF file with download method

        return $pdf->stream('noticias-por-tag.pdf');
        // return $pdf->download('noticias-por-tag.pdf');
    }

    public function exportExecel()
    {
        $data = Session::get('jornais');

        return Excel::download(
            new NewsListExport($data),
            'noticias-por-tag-'.Session::get('Filter.tag').'.xlsx'
        );
    }

    private function postListTags($tagName, $url, $jornaisKey)
    {
        $ids = $this->taglistId($tagName, $url);
        if ((bool) $ids) {
            foreach ($ids as $key => $id) {
                $this->postTagList($id, $url, $jornaisKey, $tagName);
            }
        }
    }

    private function taglistId($tagName, $url)
    {
        $url2 = $url.'/wp-json/wp/v2/tags';
        $ids = [];
        if ($this->checkUrl($url2)) {
            try {
                $responseTagId = Http::withUserAgent($this->userAgent)
                    ->get($url2, [
                        'search' => $tagName,
                    ]);

                $jsonTagIds = $responseTagId->json();

                if ((bool) $jsonTagIds) {
                    foreach ($jsonTagIds as $value) {
                        if (isset($value['id'])) {
                            $ids[] = $value['id'];
                        }
                    }
                }
            } catch (\Exception $e) {
                $this->flash_data[] = [
                    'text' => 'Não está sendo possivel acessar API wp-json  no dominio '.$url,
                    'status' => 'warning',
                ];

                Log::error('responseTagId '.$url, [$e]);
            }
        }

        return $ids;
    }

    private function postTagList($tag_id, $url, $jornaisKey, $tagName)
    {
        $dateAfter = Session::get('Filter.date_after').'T00:00:00'; // '2024-01-29T15:53:12';
        $dateBefore = Session::get('Filter.date_before').'T23:59:59'; // '2024-01-29T15:53:12';

        $dateAfter = \Carbon\Carbon::parse($dateAfter);
        $dateBefore = \Carbon\Carbon::parse($dateBefore);

        $date_after = $dateAfter->format('Y-m-d').'T00:00:00';
        $date_before = $dateBefore->format('Y-m-d').'T23:59:59';

        try {
            $response = Http::withUserAgent($this->userAgent)
                ->get($url.'/wp-json/wp/v2/posts', [
                    'tags' => $tag_id,
                    'type' => 'post',
                    'per_page' => 50,
                    'after' => $date_after,
                    'before' => $date_before,
                ]);
            $posts = $response->json();
            if ((bool) $posts) {
                $news = [];
                $countNews = 0;
                foreach ($posts as $value) {
                    $dataFeedReader = [
                        'user_id' => $user_id['id'] ?? 1,
                        'tag' => $tagName,
                        'created_at' => date('Y-m-d h:i:s'),
                    ];

                    $valueDate = \Carbon\Carbon::parse($value['date']);
                    $this->jornais[$jornaisKey]['news'][] = [
                        'title' => $value['title']['rendered'],
                        'date' => $valueDate->format('d/m/Y H:s'),
                        'link' => $value['link'],
                    ];

                    $Newspaper = Newspaper::find($this->jornais[$jornaisKey]['id']);

                    $n = $Newspaper->FeedNewsList()
                        ->where('link', $value['link'])
                        ->first();

                    if (!$n) {
                        $news['newspaper_id'] = $value['id'];
                        $news['tag'] = $tagName;
                        $news['title'] = $value['title']['rendered'];
                        $news['link'] = $value['link'];
                        $news['date'] = $value['date'];

                        $feedNews = new FeedNewsList($news);

                        $Newspaper->FeedNewsList()->save($feedNews);
                        ++$countNews;
                    }

                    $dataFeedReader['updated_at'] = date('Y-m-d h:i:s');
                    $dataFeedReader['count'] = $countNews;

                    $ModelsFeedReader = new ModelsFeedReader($dataFeedReader);
                    $ModelsFeedReader->save();
                }
                Session::put('haveNews', true);
            }

            // dd($response);
        } catch (\Exception $e) {
            Log::error('postTagList '.$url, [$e]);
        }

        Session::put('jornais', $this->jornais);
    }

    protected function getJson($url)
    {
        $response = file_get_contents($url, false);

        return json_decode($response);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'tag' => 'required',
        ]);

        // limpar sessão
        $this->clearFilter();
        // https://www.h2foz.com.br/wp-json/wp/v2/tags?search=GOVBR
        // obter tag_id
        $tag = strtolower($request->all('tag')['tag']);
        $date_after = $request->all('date_after')['date_after'];
        $date_before = $request->all('date_before')['date_before'];
        Session::put('Filter.tag', $tag);

        if (!$date_after) {
            $date_after = date('Y-m-01');
        }

        if (!$date_before) {
            $date_before = date('Y-m-d');
        }

        Session::put('Filter.date_after', $date_after);
        Session::put('Filter.date_before', $date_before);
        foreach ($this->jornais as $key => $value) {
            $this->postListTags($tag, $value['url'], $key);
        }

        Session::flash('flash_data', $this->flash_data);

        return view(
            'news-list',
            [
                'jornais' => Session::get('jornais'),
                'tag' => Session::get('Filter.tag'),
            ]
        );
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
    }

    public function clearFilter()
    {
        Session::put('haveNews', false);
        Session::put('Filter.tag', '');
        Session::put('Filter.date_after', '');
        Session::put('Filter.date_before', '');
        Session::put('jornais', []);

        return redirect()->route('news-list.index');
    }

    public function checkUrl($url)
    {
        // Check if the URL exists
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $error = curl_error($ch);

        curl_close($ch);

        return $response == 'true';
    }
}
