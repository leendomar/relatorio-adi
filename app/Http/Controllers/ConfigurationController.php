<?php

namespace App\Http\Controllers;

use App\Models\FeedSyncRangeDateLog;
use App\Models\Newspaper;
use App\Services\FeedSyncService;
use Illuminate\Http\Request;

// use Illuminate\Support\Facades\Session;

class ConfigurationController extends Controller
{
    protected $feedSyncService;

    public function __construct(FeedSyncService $feedSyncService)
    {
        $this->feedSyncService = $feedSyncService;
    }

    public function feedDateRange(Request $request)
    {
        $showJournals = $request->input('showJournals');

        session()->put('Filter.feedDateRange.showJournals', $showJournals);

        if (!session()->get('Filter.feedDateRange.start_date')) {
            $last24Hours = today()->subDay()->format('Y-m-d');
            session()->put('Filter.feedDateRange.start_date', $last24Hours);
        }

        if (!session()->get('Filter.feedDateRange.end_date')) {
            $now = today()->format('Y-m-d');
            session()->put('Filter.feedDateRange.end_date', $now);
        }

        $start_date = session()->get('Filter.feedDateRange.start_date');
        $end_date = session()->get('Filter.feedDateRange.end_date');

        $journal_ids = session()->get('Filter.feedDateRange.journal_ids') ?? $this->getNewspeperList(true);
        $newspapers = $this->getNewspeperList();

        foreach ($newspapers as $key => $newspaper) {
            // $journal_ids[] = $newspaper['id'];
            // $newspapers
            $logs = FeedSyncRangeDateLog::with('newspaper')
            ->whereIn('newspaper_id', $journal_ids)
            ->where('start_date', '>=', $start_date)
            // ->where('end_date', '<=', $end_date)
            ->orderBy('newspaper_id', 'asc');
        }

        if (request()->ajax()) {
            return \DataTables::of($logs)
                ->addColumn('status', function ($log) {
                    return $log->status;
                })
                ->addColumn('error_message', function ($log) {
                    return $log->error_message ?? '—';
                })
                ->addColumn('updated_at', function ($log) {
                    return $log->updated_at->format('d/m/Y H:i');
                })
                ->make(true);
        }

        return view('configuration.feed-date-range', [
            'logs' => $logs->get(),
            'journalList' => $this->getNewspeperList(),
        ]);
    }

    public function trackDateRange(Request $request)
    {
        $startDate = $request->input('start_date', null);
        session()->put('Filter.feedDateRange.start_date', $startDate);

        $endDate = $request->input('end_date', null);
        session()->put('Filter.feedDateRange.end_date', $startDate);

        $journal_ids = $request->input('journal_ids', null);

        session()->put('Filter.feedDateRange.journal_ids', $journal_ids);

        if (!$startDate || !$endDate) {
            return back()->withErrors(['error' => 'Datas inválidas fornecidas.']);
        }
    }

    public function getNewspeperList($ids = false)
    {
        $newspapers = Newspaper::where('status', 'ativo')
        ->where('wp_active', 1)
        ->select('id', 'name', 'url')
        ->get()->toArray();
        if ($ids) {
            foreach ($newspapers as $key => &$journal) {
                $journal = $journal['id'];
            }
        }

        return $newspapers;
    }

    public function storeFeedByDateRangeJob(Request $request)
    {
        $startDate = $request->input('start_date', null);
        session()->put('Filter.feedDateRange.start_date', $startDate);

        $endDate = $request->input('end_date', null);
        session()->put('Filter.feedDateRange.end_date', $endDate);

        $journal_ids = $request->input('journal_ids', null);
        if (!$journal_ids) {
            $journal_ids = $this->getNewspeperList(true);
        }

        session()->put('Filter.feedDateRange.journal_ids', $journal_ids);

        $result = $this->feedSyncService->processDateRange($startDate, $endDate, $journal_ids);

        if (isset($result['error'])) {
            return back()->withErrors(['error' => $result['error']]);
        }

        return redirect()->back()->with('success', $result['success']);
    }
}
