<?php

namespace App\Services;

use App\Jobs\ProcessFeedByDateRangeJob;
use App\Models\FeedSyncRangeDateLog;
use App\Models\Newspaper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class FeedSyncService
{
    public function processDateRange($startDate, $endDate, $journal_ids)
    {
        if (!$journal_ids) {
            return ['error' => 'Erro: Selecione ao menos um jornal'];
        }

        $newspapers = Newspaper::where('status', 'ativo')
            ->where('wp_active', 1)
            ->whereIn('id', $journal_ids)
            ->select('id', 'name', 'url')
            ->get();

        foreach ($newspapers as $newspaper) {
            $this->processNewspaper($newspaper, $startDate, $endDate);
        }

        return ['success' => 'Processamento iniciado com sucesso.'];
    }

    public function processNewspaper($newspaper, $startDate, $endDate, $queue = 'default')
    {
        try {
            $startDate = Carbon::createFromFormat('Y-m-d', $startDate);
            $endDate = Carbon::createFromFormat('Y-m-d', $endDate);

            // Validar se o endDate não é maior que hoje
            // if ($endDate->greaterThan(today())) {
            //     $endDate = today();
            // }
        } catch (\Exception $e) {
            Log::error('Erro ao converter datas:', ['exception' => $e->getMessage()]);

            return ['error' => 'Formato de data inválido.'];
        }

        $currentDate = clone $startDate;

        while ($currentDate->lte($endDate)) {
            $nextDate = (clone $currentDate)->addDay();

            if ($this->logExists($newspaper->id, $currentDate, $nextDate)) {
                Log::info("Já existe um processamento pendente para {$newspaper->name} de {$currentDate->format('Y-m-d')} até {$nextDate->format('Y-m-d')}");
            } else {
                $this->createLogAndDispatchJob($newspaper, $currentDate, $nextDate, $queue);
            }

            $currentDate->addDay();
        }
    }

    private function logExists($newspaperId, $startDate, $endDate)
    {
        return FeedSyncRangeDateLog::where('newspaper_id', $newspaperId)
            ->where('start_date', $startDate->format('Y-m-d'))
            ->where('end_date', $endDate->format('Y-m-d'))
            ->whereIn('status', ['pendente', 'processando'])
            ->exists();
    }

    private function getFeedSyncRangeDateLog($newspaper, $startDate, $endDate)
    {
        return FeedSyncRangeDateLog::updateOrCreate([
            'newspaper_id' => $newspaper->id,
            'start_date' => $startDate->format('Y-m-d'),
            'end_date' => $endDate->format('Y-m-d'),
        ], [
            'status' => 'pendente',
            'error_message' => '',
        ]);
    }

    private function createLogAndDispatchJob($newspaper, $startDate, $endDate, $queue)
    {
        $log = $this->getFeedSyncRangeDateLog($newspaper, $startDate, $endDate);

        dispatch(new ProcessFeedByDateRangeJob(
            $newspaper,
            $startDate->format('Y-m-d'),
            $endDate->format('Y-m-d'),
            $log->id
        ))->onQueue($queue);
    }
}
