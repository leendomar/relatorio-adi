<?php

namespace App\Services;

use App\Models\FeedNewsList;
use App\Models\Tag;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Vedmant\FeedReader\Facades\FeedReader;

class FeedAdiService
{
    public $wp_version;

    protected $tags = [];

    protected $params = '';

    public function __construct()
    {
        $this->tags = Tag::where('is_active', 1)->pluck('name')->toArray();
    }

    public function processFeedUrls($newspaper)
    {
        $feedUrlList = $this->generateFeedUrls($newspaper);

        return $this->fetchAndProcessFeeds($feedUrlList);
    }

    public function processFeedByDateRangeUrls($newspaper, $start_date, $end_date)
    {
        if ($start_date) {
            $this->params .= '&start_date='.$start_date;
        }

        if ($end_date) {
            $this->params .= '&end_date='.$end_date;
        }

        $feedUrlList = $this->generateFeedUrls($newspaper);

        return $this->fetchAndProcessFeeds($feedUrlList);
    }

    private function generateFeedUrls($newspaper)
    {
        $feedUrlList = [];
        $newspaperUrl = $newspaper['url'];

        if ($this->isPluginInstalled($newspaperUrl)) {
            foreach ($this->tags ?? [] as $tag) {
                $tagSlug = urlencode($tag);
                $baseUrl = rtrim($newspaper['url'], '/');
                $feedUrl = "{$baseUrl}?feed=adi-feed&term={$tagSlug}{$this->params}";
                $feedUrlList[] = [
                    'newspaper_id' => $newspaper['id'],
                    'tag' => $tag,
                    'feed_url' => $feedUrl,
                    'wp_version' => $this->wp_version,
                ];
            }
        }

        return $feedUrlList;
    }

    private function fetchAndProcessFeeds($feedUrlList)
    {
        $results = [];
        $errors = [];
        $headers = ['User-Agent' => 'WPP-ADI-FEED/1'];

        // Criar cliente Guzzle fora do loop para reuso
        $client = new Client([
            'allow_redirects' => false, // Não permite redirecionamentos
            'timeout' => 10, // Tempo limite para evitar travamentos
        ]);

        foreach ($feedUrlList ?? [] as $item) {
            try {
                // Tenta obter os feeds
                $feeds = FeedReader::read($item['feed_url'], 'default', $headers, $client);

                // Validar se os feeds foram carregados corretamente
                if (!$feeds || !method_exists($feeds, 'get_items')) {
                    throw new \Exception('Feed inválido ou sem itens');
                }

                $items = $feeds->get_items();

                if (!$items) {
                    throw new \Exception('Feed carregado, mas sem itens disponíveis');
                }
                $results[] = $this->processFeedItems($feeds, $item);
            } catch (RequestException $e) {
                $error = [
                    'feed_url' => $item['feed_url'],
                    'error' => 'Erro de conexão: '.$e->getMessage(),
                ];
                $errors[] = $error;
                Log::error('Falha ao conectar ao feed', $e / rror);
            } catch (\Exception $e) {
                $error = [
                    'feed_url' => $item['feed_url'],
                    'error' => $e->getMessage(),
                ];
                $errors[] = $error;
                Log::error('Erro ao processar feed', $error);
            }
        }

        return ['results' => $results, 'errors' => $errors];
    }

    private function processFeedItems($feed, $item)
    {
        $feedSaved = [];
        $errors = [];
        $countNews = 0;
        $tag = $item['tag'];

        $feedItems = $feed->get_items() ?? [];
        if ($this->tags) {
            foreach ($feedItems as $feedItem) {
                try {
                    // Obtém as categorias do item
                    $categories = array_map(fn ($c) => trim($c->get_label()), $feedItem->get_categories() ?? []);
                    $lowerCategories = array_map('strtolower', $categories);
                    // Verifica se pelo menos uma categoria permitida está presente
                    if (!in_array(strtolower($tag), $lowerCategories)) {
                        continue; // Ignora o item se não tiver categorias permitidas
                    }

                    $title = $feedItem->get_title();
                    $pubDate = $feedItem->get_date('Y-m-d H:i:s');
                    $link = $this->removeUrlQuery($feedItem->get_link());

                    $viewTags = $feedItem->get_item_tags('http://purl.org/dc/elements/1.1/', 'view');
                    if (!$viewTags) {
                        $viewTags = $feedItem->get_item_tags('', 'view'); // Tenta sem namespace
                    }

                    $stayTags = $feedItem->get_item_tags('http://purl.org/dc/elements/1.1/', 'stay');
                    if (!$stayTags) {
                        $stayTags = $feedItem->get_item_tags('', 'stay'); // Tenta sem namespace
                    }

                    // Verifica se há dados antes de acessar
                    $view = !empty($viewTags[0]['data']) ? $viewTags[0]['data'] : null;
                    $stay = !empty($stayTags[0]['data']) ? $stayTags[0]['data'] : null;

                    $data = [
                        'newspaper_id' => $item['newspaper_id'],
                        'tag' => $item['tag'],
                        'title' => $title,
                        'date' => $pubDate,
                        'link' => $link,
                        'view' => $view,
                        'stay' => $stay,
                    ];

                    // Log::info('data', $data);

                    $feedSaved[] = FeedNewsList::updateOrCreate(
                        [
                            'newspaper_id' => $data['newspaper_id'],
                            'tag' => $data['tag'],
                            'title' => $data['title'],
                        ],
                        [
                            'date' => $data['date'],
                            'link' => $data['link'],
                            'view' => $data['view'],
                            'stay' => $this->convertToFloat($data['stay']),
                        ]
                    );

                    ++$countNews;
                } catch (\Exception $e) {
                    $errors[] = [
                        'feed_url' => $item['feed_url'],
                        'error' => $e->getMessage(),
                        'categories' => $categories, // Adiciona categorias no log para depuração
                    ];

                    Log::error('Erro ao processar item do feed', [
                        'feed_url' => $item['feed_url'] ?? null,
                        'exception' => $e->getMessage(),
                        'categories' => $categories,
                    ]);
                }
            }
        } else {
            Log::error('processFeedItems: sNem uma tag está listada', [
                'item' => $item,
            ]);
        }

        return [
            'saved' => $feedSaved,
            'errors' => $errors,
            'count' => $countNews,
        ];
    }

    public function convertToFloat($value)
    {
        // Remove as vírgulas e converte para float
        $value = str_replace(',', '', $value);

        return (float) $value;
    }

    private function removeUrlQuery($url)
    {
        $parsedUrl = parse_url($url);

        return isset($parsedUrl['scheme'], $parsedUrl['host'], $parsedUrl['path'])
            ? "{$parsedUrl['scheme']}://{$parsedUrl['host']}{$parsedUrl['path']}"
            : $url;
    }

    public function isPluginInstalled($feedUrl)
    {
        try {
            // Fazer a requisição para o feed
            $response = Http::withHeaders([
                'User-Agent' => 'WPP-ADI-FEED/1',
            ])->timeout(30)->get($feedUrl.'?feed=adi-feed-version');

            $this->wp_version = '';
            // Verificar se a requisição foi bem-sucedida
            if ($response->successful()) {
                $xmlString = $response->body();

                // Garantir que o XML não esteja vazio
                if (empty($xmlString)) {
                    return false;
                }

                // Carregar o XML com controle de erros
                libxml_use_internal_errors(true);
                $xml = simplexml_load_string($xmlString);
                libxml_clear_errors();
                if ($xml === false) {
                    return false; // XML inválido
                }

                // Verificar se a tag <slug> contém "wpp-adi-feed"
                $this->wp_version = $xml->version;

                return isset($xml->slug) && (string) $xml->slug === 'wpp-adi-feed';
            }
        } catch (\Exception $e) {
            // Captura qualquer erro da requisição HTTP
            return false;
        }

        return false; // Caso a requisição falhe
    }

    public function getFeedXml($feedUrl)
    {
        $headers = [
            'User-Agent' => 'WPP-ADI-FEED/1',
        ];

        $client = new Client([
            'allow_redirects' => false, // Não permite redirecionamentos
            'timeout' => 10, // Tempo limite para evitar travamentos
        ]);

        $feed = FeedReader::read($feedUrl, 'default', $headers, $client);

        return response($feed->get_items(), 200)
            ->header('Content-Type', 'application/xml');
    }
}
