<?php

namespace App\Jobs;

use App\Http\Controllers\FeedWppAdiFeedController;
use App\Models\Newspaper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessNewspaperFeed implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $tries = 5;
    public $retryAfter = 60;
    public $timeout = 30;

    protected $newspaper;

    /**
     * Cria uma nova instância do Job.
     *
     * @return void
     */
    public function __construct(Newspaper $newspaper)
    {
        $this->newspaper = $newspaper;
    }

    /**
     * Executa o Job.
     *
     * @return void
     */
    public function handle()
    {
        // Instancia o controlador FeedWppAdiFeedController
        $controller = new FeedWppAdiFeedController();
        // Chama a função feedUrl para processar o feed
        $controller->feedUrl($this->newspaper->toArray());
    }
}
