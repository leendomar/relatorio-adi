<?php

namespace App\Jobs;

use App\Services\FeedAdiService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessFeedAdiJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $tries = 5;
    public $retryAfter = 60;
    public $timeout = 30;

    protected $newspaper;

    public function __construct($newspaper)
    {
        $this->newspaper = $newspaper;
    }

    public function handle(FeedAdiService $feedAdiService)
    {
        $feedAdiService->processFeedUrls($this->newspaper);
    }
}
