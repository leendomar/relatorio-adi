<?php

namespace App\Jobs;

use App\Models\FeedNewsList;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateNewsStatsJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $url;
    protected $time_spent;

    /**
     * Create a new job instance.
     */
    public function __construct($url, $time_spent)
    {
        $this->url = $url;
        $this->time_spent = $time_spent; // Garante que será um inteiro
    }

    public function handle(): void
    {
        $news = FeedNewsList::where('link', $this->url)->first();
    
        if (!$news) {
            Log::warning("Notícia não encontrada para URL {$this->url}");
            return;
        }
    
        $views = $news->view ?? 0;
        $stay = $news->stay ?? 0;
    
        // Regra 1: Se menos de 10 visualizações, ignora tempos > 300s
        if ($views < 10 && $this->time_spent > 300) {
            Log::info("Tempo muito alto para URL {$this->url} - Ignorado.");
            $this->time_spent = 0;
        }
    
        // Regra 2: Se >= 10 visualizações, ignora tempos 50% acima da média
        if ($views >= 10) {
            $media_atual = $views > 0 ? $stay / $views : 0;
            if ($this->time_spent > $media_atual * 1.5) {
                Log::info("Tempo acima da média para URL {$this->url} - Ignorado.");
                $this->time_spent = 0;
            }
        }
    
        // Verifica duplicidade (atualização recente)
        $recentEntry = FeedNewsList::where('link', $this->url)
            ->where('updated_at', '>=', now()->subMinutes(5))
            ->exists();
    
        if ($recentEntry) {
            Log::info("Entrada duplicada ignorada para {$this->url}");
            return;
        }
    
        // Atualiza os valores CORRETAMENTE
        $views++; // Incrementa ANTES de usar
        $news->view = $views;
        $news->stay = ($stay + $this->time_spent) / $views; // Usa o $views atualizado
        $news->save();
    
        Log::info("Estatísticas atualizadas para URL {$this->url}", [$news]);
    }
    
    
}
