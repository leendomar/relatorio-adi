<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SendDiscordWebhook implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $message;
    public $type;
    public $content;
    private $webhookUrl;

    public $tries = 3; // Número máximo de tentativas
    public $timeout = 30; // Tempo máximo de execução em segundos
    public $retryAfter = 60;

    /**
     * Create a new job instance.
     */
    public function __construct(string $type, string $message, ?array $content = null)
    {
        $this->message = $message;
        $this->content = $content;
        $this->type = $type;
        $this->webhookUrl = env('LOG_DISCORD_WEBHOOK_URL');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Verifica se o envio está habilitado
        if (!env('LOG_DISCORD_WEBHOOK_SEND')) {
            // Log::info('Envio para o Discord desativado via configuração do .env.');

            return;
        }

        // Verifica se a URL do webhook está configurada
        if (!$this->webhookUrl) {
            Log::error('URL do webhook Discord não está configurada no .env.');

            return;
        }

        try {
            $payload = [
                'content' => $this->message,
            ];

            // Adiciona conteúdo adicional, se existir
            if (!empty($this->content)) {
                $payload = array_merge($payload, $this->content);
            }

            $response = Http::post($this->webhookUrl, $payload);

            if ($response->status() == 429) {
                $retryAfter = $response->json('retry_after', 1000); // Tempo de espera em milissegundos
                // Log::warning('Rate limit atingido. Reenviando em '.($retryAfter / 1000).' segundos.');

                // Reencaminhar para a fila com delay
                $this->release($retryAfter / 1000); // Convertendo milissegundos para segundos
            } elseif (!$response->successful()) {
                $this->logMessage('Falha no envio para o Discord: '.$response->body());
            }
        } catch (\Exception $e) {
            $this->logMessage('Erro no Job SendDiscordWebhook: '.$e->getMessage());
        }
    }

    /**
     * Mapear o tipo de log e registrar a mensagem.
     *
     * @return void
     */
    private function logMessage(string $message)
    {
        $logTypes = ['info', 'warning', 'error', 'debug', 'notice'];
        $type = in_array($this->type, $logTypes) ? $this->type : 'error'; // Padrão será 'error'

        Log::{$type}($message, $this->content ?? []);
    }
}
