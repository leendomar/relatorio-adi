<?php

namespace App\Jobs;

use App\Models\FeedSyncRangeDateLog;
use App\Services\FeedAdiService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ProcessFeedByDateRangeJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $tries = 5;
    public $retryAfter = 60;
    public $timeout = 60;

    protected $newspaper;
    protected $start_date;
    protected $end_date;
    protected $logId;

    public function __construct($newspaper, $start_date, $end_date, $logId)
    {
        $this->newspaper = $newspaper;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->logId = $logId;
    }

    public function handle(FeedAdiService $feedAdiService)
    {
        $log = FeedSyncRangeDateLog::find($this->logId);

        if (!$log) {
            Log::error('Log de sincronização não encontrado para ID: '.$this->logId);

            return;
        }

        try {
            $log->update(['status' => 'processando']);
            $result = $feedAdiService->processFeedByDateRangeUrls($this->newspaper, $this->start_date, $this->end_date);
            $log->update([
                'status' => 'concluído',
                'error_message' => $result['errors'] ?? '',
            ]);
        } catch (\Exception $e) {
            $log->update([
                'status' => 'erro',
                'error_message' => $e->getMessage(),
            ]);
            Log::error('Erro no processamento do feed:', ['exception' => $e->getMessage()]);
        }
    }
}
