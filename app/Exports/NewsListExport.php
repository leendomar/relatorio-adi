<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class NewsListExport implements FromArray, WithHeadings, WithDrawings
{
    private $data;

    public function __construct(array $data)
    {
        $this->data = $this->formatData($data);
    }

    public function array(): array
    {
        return $this->data;
    }

    public function headings(): array
    {
        return ['#', 'Jornal', 'Título', 'Link', 'Data'];
    }

    /**
     * Adiciona uma imagem no cabeçalho do Excel.
     */
    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('Logo do Relatório');
        $drawing->setPath(public_path('logo.png')); // Caminho da imagem
        $drawing->setHeight(50);
        $drawing->setCoordinates('A1'); // Posição no Excel

        return $drawing;
    }

    /**
     * Formata os dados para agrupar as notícias por jornal.
     */
    private function formatData($data)
    {
        $formattedData = [];
        $index = 1;

        foreach ($data as $journal => $news) {
            // Adiciona uma linha separadora com o nome do Jornal
            $formattedData[] = [$index++, strtoupper($journal), '', '', ''];

            foreach ($news as $newsItem) {
                $formattedData[] = [
                    '',
                    '',
                    $newsItem['title'],
                    $newsItem['link'],
                    $newsItem['date'],
                ];
            }
        }

        return $formattedData;
    }
}
