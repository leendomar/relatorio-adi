<?php

namespace App\Exports;

use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\BeforeSheet;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Font;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class FeedNewsListExport implements FromArray, WithHeadings, WithStyles, WithColumnFormatting, WithEvents, ShouldAutoSize
{
    protected $feedJornais;
    protected $cliente;
    protected $date_after;
    protected $date_before;
    protected $tag;
    protected $obs;
    protected $view;
    protected $stay;

    public function __construct($feedJornais, $tag, $date_after, $date_before, $cliente, $obs, $view, $stay)
    {
        $this->feedJornais = $feedJornais;
        $this->tag = $tag;
        $this->date_after = $date_after;
        $this->date_before = $date_before;
        $this->cliente = $cliente;
        $this->obs = $obs;
        $this->view = $view;
        $this->stay = $stay;
    }

    public function array(): array
    {
        $data = [];
        $line = 1;
        foreach ($this->feedJornais as $key1 => $jornal) {
            if (!empty($jornal['news'])) {
                // Nome do jornal e cidade
                $i =  [
                    'Jornal' => strtoupper($jornal['name']).' - '.strtoupper($jornal['city']),
                ];
                    if ($this->view) {
                        $i['V.'] = 'V.';
                    }

                    }

                $data[] = $i;

                foreach ($jornal['news'] as $key2 => $news) {
                    $n = [
                        '#' => $line++,
                        'Data' => Carbon::parse($news['date'])->format('d/m/Y H:i'),
                        'Título' => $news['title'] ? '=HYPERLINK("'.$news['link'].'", "'.$news['title'].'")' : '',
                    ];
                    if ($this->view) {
                        $n['V'] = $news['view'] ?? '';
                    }
                    if ($this->stay) {
                        $n['P'] = $news['stay'] ? number_format($news['stay'] / 60, 2) : '0.00';                    
                    }
                    $data[] = $n;
                }
            }
        }

        return $data;
    }
    
    public function headings(): array
    {
        $data = [
            ['', '', 'Relatório de Publicações on-line'], // Título do relatório
            ['', '', 'Cliente: '.$this->cliente], // Nome do cliente
            ['', '', 'Período: '.$this->date_after.' a '.$this->date_before], // Intervalo de datas
            ['', '', 'TAG: '.$this->tag], // TAG do relatório
            ['', '', ''], // Linha vazia para separação
        ];
    
        // Definição dos cabeçalhos da tabela
        if ($this->view && $this->stay) {
            $data[] = ['#', 'Data', 'Título', 'V.', 'P.'];
        } elseif ($this->view) {
            $data[] = ['#', 'Data', 'Título', 'V.'];
        } elseif ($this->stay) {
            $data[] = ['#', 'Data', 'Título', 'P.'];
        } else {
            $data[] = ['#', 'Data', 'Título']; // Garantia de um cabeçalho válido
        }
    
        return $data;
    }
    

    public function styles(Worksheet $sheet)
    {
        // Ajusta a logo ocupando A1:B6
        $sheet->mergeCells('A1:B6');

        $linha = 8; // Ajustando a posição após os cabeçalhos

        // Estilizar as células de nome do jornal para ocupar toda a linha
        foreach ($this->feedJornais as $jornal) {
            if (!empty($jornal['news'])) {
                $sheet->mergeCells("A{$linha}:C{$linha}");
                $sheet->getStyle("A{$linha}")
                    ->getFont()->setBold(true)
                    ->getColor()->setARGB('000000');
                $sheet->getStyle("A{$linha}")
                    ->getFill()->setFillType('solid')->getStartColor()->setARGB('D3D3D3');
                $sheet->getStyle("A{$linha}")
                    ->getAlignment()->setHorizontal('center');

                // Cores de fundo para as colunas D e E
                $sheet->getStyle("D{$linha}")
                    ->getFill()->setFillType('solid')->getStartColor()->setARGB('D3D3D3');
                $sheet->getStyle("D{$linha}")->getFont()->getColor()->setARGB('000000');

                $sheet->getStyle("E{$linha}")
                    ->getFill()->setFillType('solid')->getStartColor()->setARGB('D3D3D3');
                $sheet->getStyle("E{$linha}")->getFont()->getColor()->setARGB('000000');

                $linha += count($jornal['news']) + 1;
            }
        }

        $sheet->getStyle('B')->getAlignment()->setHorizontal('center');
        // Ajuste na coluna B para 6 cm
        $sheet->getColumnDimension('B')->setWidth(7 * 5.4); // Aproximadamente 6 cm

        // Estilização do título e cabeçalho
        $sheet->getStyle('C2:C5')->getFont()->setBold(true);
        $sheet->getStyle('C2:C5')->getAlignment()->setHorizontal('left');

        // Adicionando hyperlinks às células da coluna C
        foreach ($sheet->getRowIterator(9) as $row) {
            $rowIndex = $row->getRowIndex();
            $cell = 'C'.$rowIndex;
            if ($sheet->getCell('D'.$rowIndex)->getValue()) {
                $sheet->getCell($cell)->getHyperlink()->setUrl($sheet->getCell('D'.$rowIndex)->getValue());
                $sheet->getStyle($cell)->applyFromArray([
                    'font' => ['color' => ['rgb' => Color::COLOR_BLUE], 'underline' => Font::UNDERLINE_SINGLE],
                ]);
            }
        }

        return [];
    }

    public function columnFormats(): array
    {
        return [
            'A' => '0', // Número inteiro
            'B' => 'dd/mm/yyyy hh:mm', // Formato de data
            'C' => '@', // Texto (útil para links)
            'E' => '#,##0.00', // Formato numérico decimal
            'F' => '#,##0.00', // Formato numérico decimal
        ];
    }

    public function registerEvents(): array
    {
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $drawing = new Drawing();
                $drawing->setName('Logo');
                $drawing->setDescription('Logo da empresa');
                $drawing->setPath(public_path('/storage/img/logo-adi.png'));
                $drawing->setHeight(100);
                $drawing->setCoordinates('A1');
                $drawing->setWorksheet($event->sheet->getDelegate());
            },
        ];
    }
}
