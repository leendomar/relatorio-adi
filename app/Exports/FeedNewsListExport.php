<?php

namespace App\Exports;

use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Illuminate\Support\Facades\Log;

class FeedNewsListExport implements FromArray, WithHeadings, WithStyles, WithColumnFormatting, ShouldAutoSize
{
    protected $feedJornais;
    protected $view;
    protected $stay;

    public function __construct($feedJornais, $view = false, $stay = false)
    {
        $this->feedJornais = $feedJornais;
        $this->view = $view;
        $this->stay = $stay;
    }

    public function array(): array
    {
        $data = [];
        $line = 1;

        foreach ($this->feedJornais as $jornal) {
            if (!empty($jornal['news'])) {
                foreach ($jornal['news'] as $news) {
                    $row = [
                        '#' => $line++,
                        'Data' => Carbon::parse($news['date'])->format('d-m-Y'),
                        'Cidade' => ucfirst(mb_convert_encoding($jornal['city'], 'UTF-8', 'auto')),
                        'Portal' => ucfirst(mb_convert_encoding($jornal['name'], 'UTF-8', 'auto')),
                        'Título' => '=HYPERLINK("'.$news['link'].'", "'.mb_convert_encoding($news['title'], 'UTF-8', 'auto').'")',
                    ];
                    
                    if ($this->view) {
                        $row['V.'] = $news['view'] ?? 0;
                    }

                    if ($this->stay) {
                        $row['P.'] = isset($news['stay']) ? number_format($news['stay'] / 60, 2, ',', '') : '0,00';
                    }

                    $data[] = $row;
                }
            }
        }

        return $data;
    }

    public function headings(): array
    {
        $headings = ['#', 'Data', 'Cidade', 'Portal', 'Título'];

        if ($this->view) {
            $headings[] = 'V.';
        }

        if ($this->stay) {
            $headings[] = 'P.';
        }

        return $headings;
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:E1')->getFont()->setBold(true);

        Log::info('test', [$this->view]);
        if ($this->view) {
            $sheet->getStyle('F1')->getFont()->setBold(true);
        }

        if ($this->stay) {
            $sheet->getStyle('G1')->getFont()->setBold(true);
        }

        return [];
    }

    public function columnFormats(): array
    {
        $formats = [
            'A' => '0', // Número inteiro
            'B' => 'dd-mm-yyyy', // Formato de data
            'C' => '@', // Texto
            'D' => '@', // Texto
            'E' => '@', // Texto com hyperlink
        ];

        if ($this->view) {
            $formats['F'] = '0'; // Número inteiro (V.)
        }

        if ($this->stay) {
            $formats['G'] = '#,##0.00'; // Decimal (P.)
        }

        return $formats;
    }
}
