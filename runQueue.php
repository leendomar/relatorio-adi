<?php

// Carregar o autoload do Composer
require __DIR__ . '/vendor/autoload.php';

// Bootstrap do Laravel
$app = require_once __DIR__ . '/bootstrap/app.php';

// Definir o ambiente do Laravel
$app->loadEnvironmentFrom('.env');

try {
    // Obter a instância do Kernel do Laravel
    $kernel = $app->make(Illuminate\Contracts\Console\Kernel::class);

    // Executar o comando queue:work
    $exitCode = $kernel->call('queue:work', [
        '--queue' => 'high,default,low',
        // '--daemon' => true, // Executar em modo daemon
    ]);

// Verifique o código de saída
if ($exitCode === 0) {
    echo "Worker iniciado com sucesso!";
} else {
    echo "Erro ao iniciar o worker.";
}
    // Imprimir a saída (opcional para logs)
    echo "Comando executado com código de saída: $exitCode\n";
    
} catch (\Exception $e) {
    // Tratar erros
    echo "Erro ao executar o comando: " . $e->getMessage() . "\n";
}
