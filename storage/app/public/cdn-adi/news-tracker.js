// ADI-FEED-ANALYSTICS (Versão Melhorada)
(function () {
    'use strict';

    const config = {
        endpoint: "https://relatorios.prportais.com.br/api/feed-adi-analytics",
        minimumTime: 5000,
        version: "1.2.",
        maxHeartbeatInterval: 30000, // Novo: intervalo máximo para envios periódicos
        heartbeatInterval: 30000, // Novo: intervalo de monitoramento contínuo
        maxRetries: 3 // Novo: tentativas de reenvio
    };

    let startTime = Date.now();
    let lastActivity = Date.now();
    let sent = false;
    let heartbeatTimer;

    // Novo: Configuração dinâmica via atributos data-
    const scriptElement = document.currentScript;
    if (scriptElement) {
        config.endpoint = scriptElement.getAttribute('data-endpoint') || config.endpoint;
        config.minimumTime = parseInt(scriptElement.getAttribute('data-minimum-time')) || config.minimumTime;
    }

    // Novo: Tenta reenviar dados salvos no localStorage
    function retryFailedRequests() {
        const failedRequests = JSON.parse(localStorage.getItem('adi_failed_requests') || '[]');
        failedRequests.forEach((data, index) => {
            if (data.retries < config.maxRetries) {
                sendToServer(data.payload)
                    .then(() => {
                        failedRequests.splice(index, 1);
                        localStorage.setItem('adi_failed_requests', JSON.stringify(failedRequests));
                    })
                    .catch(() => data.retries++);
            }
        });
    }

    // Novo: Função melhorada de envio
    async function sendToServer(data) {
        try {
            await fetch(config.endpoint, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(data),
                keepalive: true
            });
        } catch (error) {
            console.error("Erro no envio:", error);
            const failedRequests = JSON.parse(localStorage.getItem('adi_failed_requests') || '[]');
            failedRequests.push({ payload: data, timestamp: Date.now(), retries: 0 });
            localStorage.setItem('adi_failed_requests', JSON.stringify(failedRequests));
        }
    }


    // Novo: Adicionando timestamp do início da sessão
    function sendData(isHeartbeat = false) {
        if (sent && !isHeartbeat) return;

        const now = Date.now();
        const timeSpent = now - startTime;
        const activeTime = now - lastActivity;

        if (timeSpent >= config.minimumTime || isHeartbeat) {
            const data = {
                url: extractUrlPart(window.location.href),
                time_spent: Math.round(timeSpent / 1000), // Em segundos
                active_time: Math.round(activeTime / 1000), // Tempo de atividade real
                referrer: document.referrer,
                script_version: config.version,
                timestamp: Math.floor(now / 1000), // Novo: Enviando timestamp Unix (segundos)
                is_heartbeat: isHeartbeat
            };

            sendToServer(data);

            if (!isHeartbeat) sent = true;
        }
    }

    /***
     *   receber: https://jornal.paranacentro.com.br/noticia/46908/alexandre-curi-assume-presidencia-da-assembleia-legislativa
     *  retornar: https://jornal.paranacentro.com.br/noticia/46908
     */
    function extractUrlPart(url) {
        const urlObj = new URL(url);
        if (urlObj.hostname === "jornal.paranacentro.com.br") {
            return urlObj.origin + urlObj.pathname.split('/').slice(0, 3).join('/');
        }
        return url;
    }

    // Novo: Monitoramento de atividade do usuário
    function trackActivity() {
        lastActivity = Date.now();
    }

    // Novo: Inicia o monitoramento contínuo
    function startHeartbeat() {
        heartbeatTimer = setInterval(() => {
            if (Date.now() - lastActivity < config.heartbeatInterval) {
                sendData(true);
            }
        }, config.heartbeatInterval);
    }

    // Eventos melhorados
    window.addEventListener('beforeunload', () => sendData());
    window.addEventListener('pagehide', () => sendData());
    window.addEventListener('visibilitychange', () => {
        if (document.visibilityState === 'hidden') sendData();
    });

    // Novo: Eventos de atividade do usuário
    document.addEventListener('click', trackActivity);
    document.addEventListener('scroll', trackActivity);
    document.addEventListener('mousemove', trackActivity);

    // Inicialização
    retryFailedRequests(); // Tenta reenviar dados pendentes
    startHeartbeat(); // Inicia monitoramento contínuo

    // Novo: Garante a limpeza ao descarregar
    window.addEventListener('unload', () => {
        clearInterval(heartbeatTimer);
        document.removeEventListener('click', trackActivity);
        document.removeEventListener('scroll', trackActivity);
    });

})();