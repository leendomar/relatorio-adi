<?php

use App\Http\Controllers\ConfigurationController;
use App\Http\Controllers\FeedNewsListController;
use App\Http\Controllers\FeedReaderController;
use App\Http\Controllers\FeedWppAdiFeedController;
use App\Http\Controllers\NewsListController;
use App\Http\Controllers\NewspaperController;
use App\Http\Controllers\NewspaperTagurlController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\WppAdiFeedController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/feed-reader', [FeedReaderController::class, 'index']);
Route::get('/feed-update-news-stats', [FeedReaderController::class, 'updateNewsStats']);

Route::get('/feed-reader', [FeedReaderController::class, 'index']);
Route::post('/feed-wp-ativar', [FeedWppAdiFeedController::class, 'wpActive']);
Route::get('/wp-active-all-domain', [FeedWppAdiFeedController::class, 'wpActiveAllDomain']);
Route::get('/feed-job', [FeedWppAdiFeedController::class, 'index']);
Route::get('/feed-direct-url', [FeedWppAdiFeedController::class, 'feedDirectUrl']);

Route::get('/feed-wp-adi-plugin-update', [WppAdiFeedController::class, 'getPluginUpdateFeed']);

Route::middleware('auth')->group(function () {
    Route::get('/plugin', [WppAdiFeedController::class, 'index']);
    Route::post('/plugin/upload', [WppAdiFeedController::class, 'upload']);
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/news-list/pdf', [NewsListController::class, 'createPDF']);
    Route::get('/news-list/excel/', [NewsListController::class, 'exportExecel']);

    Route::get('/news-list/clear', [NewsListController::class, 'clearFilter']);
    Route::resource('/news-list', NewsListController::class);

    Route::get('/configuration/feed-date-range', [ConfigurationController::class, 'feedDateRange'])->name('configuration.feed-date-range');
    Route::post('/configuration/track-feed-date-range', [ConfigurationController::class, 'trackDateRange'])->name('configuration.track-feed-date-range');
    Route::post('/configuration/store-feed-date-range', [ConfigurationController::class, 'storeFeedByDateRangeJob'])
    ->name('configuration.store-feed-date-range');

    Route::resource('/newspapers', NewspaperController::class);

    Route::post('/newspaper-tagurl/store', [NewspaperTagurlController::class, 'store'])->name('newspaper-tagurl.store');
    Route::delete('/newspaper-tagurl/destroy/{newspaperTagurl}', [NewspaperTagurlController::class, 'destroy'])->name('newspaper-tagurl.destroy');

    Route::get('/feed-news-list/clear', [FeedNewsListController::class, 'clearFilter'])->name('feed-news-list.filter');
    Route::post('/feed-news-list/pdf', [FeedNewsListController::class, 'createPDF'])->name('feed-news-list.pdf');
    Route::post('/feed-news-list/excel', [FeedNewsListController::class, 'exportExcel'])->name('feed-news-list.excel');
    Route::post('/feed-news-list/excel', [FeedNewsListController::class, 'exportExcel'])->name('feed-news-list.excel');

    Route::resource('/feed-news-list', FeedNewsListController::class);

    Route::get('tags/list', action: [TagController::class, 'list'])->name('tags.list');
    Route::resource('tags', TagController::class);
});

require __DIR__.'/auth.php';
