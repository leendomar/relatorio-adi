<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Lista de Notícias por Feed
        </h2>
    </x-slot>
    <div class="py-12" id="content">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <form action="/feed-news-list" method="POST" class="p-6" id="form-search" autocomplete="off">
                    @csrf

                    <div class="row">
                        <div class="col-md-4 form-group">
                            <x-input-label for="input-tag" :value="__('Selecione ou digite uma Tag:')" />
                            <select name="tag" id="tag_id" class="block mt-1 w-full  select2">
                                <option value="" disabled selected>Selecione uma Tag</option>
                                @foreach($tagsList as $tag)
                                    <option value="{{ $tag['name'] }}" {{ Session('Filter.feed.tag') == $tag['name'] ? 'selected' : ''}}>{{ $tag['name'] }} - {{ $tag['client'] }}</option>
                                @endforeach
                            </select>

                            <!-- <x-text-input id="input-tag" class="block mt-1 w-full" type="text" name="tag" value="" /> -->
                            <x-input-error :messages="$errors->get('tag')" class="mt-2" />
                        </div>
                        <div class="col-md-2 form-group">
                            <x-input-label for="input-date-after" :value="__('Data Inicial')" />
                            <x-text-input id="input-date-after" class="block mt-1 w-full" type="date" name="date_after"
                                value="{{ Session('Filter.feed.date_after') }}" />
                            <x-input-error :messages="$errors->get('date_after')" class="mt-2" />
                        </div>
                        <div class="col-md-2 form-group">
                            <x-input-label for="input-date-before" :value="__('Data Final')" />
                            <x-text-input id="input-date-before" class="block mt-1 w-full" type="date"
                                name="date_before" value="{{ Session('Filter.feed.date_before') }}" />
                            <x-input-error :messages="$errors->get('date_before')" class="mt-2" />
                        </div>
                        <div class="col-md-4 form-group" style="margin-top:35px;">
                            <input id="input-search" type="submit"
                                class="items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-500 focus:outline-none transition ease-in-out duration-150"
                                value="{{__('Search')}}">
                            <a class="items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-500 focus:outline-none transition ease-in-out duration-150"
                                href="{{ URL::to('/feed-news-list/clear') }}">{{__('Clear')}}</a>
                            @if(Session('haveFeedNews'))
                            <!-- <a class="btn" href="{{ URL::to('/feed-news-list/excel') }}" target="_blank">{{__('Planilha')}} - Excel</a> -->
                            <x-bladewind::button onclick="showModal('form-mode')">
                                {{__('Relatorio')}}
                            </x-bladewind::button>
                            @endisset
                        </div>
                        <div class="col-md-6 form-group">
                            <x-input-label for="input-title-terms" :value="__('Título:')" />
                            <x-text-input id="input-title-terms" class="block mt-1 w-full" type="text" name="title_terms" value="{{ Session('Filter.feed.title_terms') }}" />
                        </div>
                        <div class="col-md-12 ">
                            <label><input type="checkbox" id="showJournals" name="showJournals" {{ Session('Filter.feed.showJournals') ?'checked':''}}> Selecionar Portais</label>
                            <div class="journals-list" id="journalsList" style="display: none;">
                                <label><input type="checkbox" id="selectAll" checked> <strong>Selecionar Todos:</strong></label>
                                <div class="checkbox-container">
                                    <div class="row">
                                            @foreach ($journalList as $jornal)
                                                <div class="col-md-4">
                                                    <label>
                                                        <input type="checkbox" class="journal" name="journal_ids[]" value="{{$jornal['id']}}"
                                                            @if(Session('Filter.feed.journal_ids'))
                                                                {{in_array($jornal['id'],Session('Filter.feed.journal_ids')) ? 'checked' : '' }}
                                                            @endif
                                                        > {{ $jornal['name'] }}
                                                    </label>
                                                </div>
                                            @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="loader" id="loading">
                        <span class="loader-text"></span>
                    </div>
                </form>
                <div class="p-6 text-gray-900">
                    @include('partials.flash-messagens')
                    {{ $line = null}}
                    @if($feedJornais)
                        <table class="table table-bordered mb-5" id="display-infos">

                            @foreach ($feedJornais as $jornal)
                                <thead>
                                    <tr class="table-danger">
                                        <th colspan="5" class="jornal-name" scope="col">{{$jornal['name']}} -
                                            {{$jornal['city']}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Título</th>
                                        <th scope="col">Data</th>
                                        <th scope="col">V</th>
                                        <th scope="col">P</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($jornal['news'] as $new)
                                        <tr class="{{ $loop->even ? 'even' : 'odd' }}">
                                            <td>{{ ++$line  }}</td>
                                            <td>
                                                <a href="{{ $new['link'] }}" target="_blank">
                                                    {{ $new['title'] }}
                                                </a>
                                            </td>
                                            <td>
                                                {{ date('d/m/Y H:i', strtotime($new['date'])) }}
                                            </td>                                         
                                            <td>{{ $new['view']  }}</td>
                                            <td class="p">{{  $new['stay']?number_format($new['stay'] / 60, 2, '.', ''):0; }}</td>

                                        </tr>
                                    @endforeach
                                </tbody> 
                            @endforeach

                        </table>

                    @endif
                </div>


            </div>

        </div>
    </div>


    <x-bladewind::modal backdrop_can_close="false" name="form-mode" ok_button_action="gerarRelatorio()"
        ok_button_label="Gerar Relatório" close_after_action="false" cancel_button_label="Cancelar">
    <div class="   "> <!-- Modal Container Responsivo -->
        <label>Gerar Relatório de Publicações - PDF</label>
        <form method="post" action="feed-news-list/pdf" id="imprimir-form">
            @csrf
            @foreach($tagsList as $tag)
                @if(Session('Filter.feed.tag') == $tag['name'])
                    <label>{{ $tag['client']}}</label>
                    <x-text-input type="hidden" name="cliente" value="{{ $tag['client']}}" />
                @endif
            @endforeach
            <x-bladewind::textarea label="Observações" name="observacao" />

            <div class="flex flex-col sm:flex-row sm:gap-4 mt-4">
                <div class="flex items-center">
                    <label class="flex">
                        <input type="radio" id="type-export-pdf" name="type-export" checked> 
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <rect width="24" height="24" fill="none"/>
                            <path fill="#9b7aff" d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2m-9.5 8.5c0 .8-.7 1.5-1.5 1.5H7v2H5.5V9H8c.8 0 1.5.7 1.5 1.5zm5 2c0 .8-.7 1.5-1.5 1.5h-2.5V9H13c.8 0 1.5.7 1.5 1.5zm4-3H17v1h1.5V13H17v2h-1.5V9h3zm-6.5 0h1v3h-1zm-5 0h1v1H7z"/>
                        </svg> PDF
                    </label>
                </div>
                <div class="flex items-center mt-2 sm:mt-0">
                    <label class="flex">
                        <input type="radio" id="type-export-excel" name="type-export">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <rect width="24" height="24" fill="none"/>
                            <path fill="#9b7aff" d="M21.17 3.25q.33 0 .59.25q.24.24.24.58v15.84q0 .34-.24.58q-.26.25-.59.25H7.83q-.33 0-.59-.25q-.24-.24-.24-.58V17H2.83q-.33 0-.59-.24Q2 16.5 2 16.17V7.83q0-.33.24-.59Q2.5 7 2.83 7H7V4.08q0-.34.24-.58q.26-.25.59-.25M7 13.06l1.18 2.22h1.79L8 12.06l1.93-3.17H8.22L7.13 10.9l-.04.06l-.03.07q-.26-.53-.56-1.07q-.25-.53-.53-1.07H4.16l1.89 3.19L4 15.28h1.78m8.1 4.22V17H8.25v2.5m5.63-3.75v-3.12H12v3.12m1.88-4.37V8.25H12v3.13M13.88 7V4.5H8.25V7m12.5 12.5V17h-5.62v2.5m5.62-3.75v-3.12h-5.62v3.12m5.62-4.37V8.25h-5.62v3.13M20.75 7V4.5h-5.62V7Z"/>
                        </svg> Excel
                    </label>
                </div>
            </div>

            <div class="mt-4">
                <label class="flex items-center">
                    <input type="checkbox" id="view" name="view"> Mostrar <b>Visualizações</b>
                </label>
                <label class="flex items-center mt-2">
                    <input type="checkbox" id="stay" name="stay"> Mostrar <b>Permanência</b>
                </label>
            </div>
        </form>
    </div>
</x-bladewind::modal>


    <style>
        .loader {
            width: 60px;
            display: none;
        }

        .loader-wheel {
            animation: spin 1s infinite linear;
            border: 2px solid rgba(30, 30, 30, 0.5);
            border-left: 4px solid #fff;
            border-radius: 50%;
            height: 50px;
            margin-bottom: 10px;
            width: 50px;
        }

        .loader-text {
            color: #f00;
            font-family: arial, sans-serif;
        }

        .loader-text:after {
            content: 'Carregando';
            animation: load 2s linear infinite;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        @keyframes load {
            0% {
                content: 'Carregando';
            }

            33% {
                content: 'Carregando.';
            }

            67% {
                content: 'Carregando..';
            }

            100% {
                content: 'Carregando...';
            }
        }

        @media (min-width: 854px) {
            .lg\:w-1\/4 {
                width: 50%;
            }
        }
    </style>
    <script>
        const contentDiv = document.getElementById('content');

        document.getElementById("form-search").addEventListener("submit", function (event) {
            contentDiv.addEventListener('mouseover', function () {
                contentDiv.style.cursor = 'progress';
            });
            document.getElementById("input-search").disabled = true;
            document.getElementById("loading").style.display = "block";
            document.getElementById("display-infos").innerHTML = ''
            document.getElementById("flash-messages").innerHTML = ''
        });
    </script>
</x-app-layout>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(function () {
        var table = $('#display-infoss').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('feed-news-list.index') }}",
            columns: [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex'
            },
            {
                data: 'title',
                name: 'Título'
            },
            {
                data: 'date',
                name: 'Data'
            },
            ]
        });
    });

    gerarRelatorio = () => {
        if (validateForm('#imprimir-form')) {
            action="feed-news-list/pdf"
            action="feed-news-list/excel"

            domEl('#imprimir-form').submit();
        } else {
            return false;
        }
    }
</script>

<script>
    // Função que altera a action do formulário com base na opção selecionada
    document.querySelectorAll('input[name="type-export"]').forEach(function(radio) {
        radio.addEventListener('change', function() {
            var form = document.getElementById('imprimir-form');
            if (document.getElementById('type-export-pdf').checked) {
                form.action = 'feed-news-list/pdf'; // Muda para PDF
            } else if (document.getElementById('type-export-excel').checked) {
                form.action = 'feed-news-list/excel'; // Muda para Excel
            }
        });
    });
</script>

<!-- Adicionar CSS do Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />

<!-- Adicionar JS do Select2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js"></script>
<script>
    $(document).ready(function () {
        $('#tag_id').select2({
            theme: 'bootstrap-5',
            placeholder: "Selecione ou digite uma Tag",
            allowClear: true // Adiciona opção de limpar o campo
        });
    });
</script>



<script>
    // Exibir/ocultar a lista de jornais com base no checkbox "Mostrar Jornais"

    // Função que exibe ou oculta a lista de jornais com base no estado do checkbox "Mostrar Jornais"
    function toggleJournals() {
      var journalsList = document.getElementById('journalsList');
      var showJournalsCheckbox = document.getElementById('showJournals');

      // Verifica o estado do checkbox "showJournals" e altera o display da lista de jornais
      if (showJournalsCheckbox.checked) {
        journalsList.style.display = 'block';  // Exibe a lista de jornais
      } else {
        journalsList.style.display = 'none';   // Oculta a lista de jornais
      }
    }

    // Evento de mudança no checkbox "showJournals"
    document.getElementById('showJournals').addEventListener('change', toggleJournals);

    // Verificação se o checkbox "showJournals" está marcado ao carregar a página
    window.onload = function() {
      toggleJournals();  // Chama a função ao carregar a página para verificar o estado do checkbox
    };
    // Selecionar/deselecionar todos os checkboxes de jornais
    document.getElementById('selectAll').addEventListener('change', function() {
      var isChecked = this.checked;
      var journalCheckboxes = document.querySelectorAll('.journal');
      journalCheckboxes.forEach(function(checkbox) {
        checkbox.checked = isChecked;
      });
    });

    // Sincronizar o "Selecionar Todos" com os outros checkboxes
    document.querySelectorAll('.journal').forEach(function(checkbox) {
      checkbox.addEventListener('change', function() {
        var allChecked = true;
        var journalCheckboxes = document.querySelectorAll('.journal');
        journalCheckboxes.forEach(function(checkbox) {
          if (!checkbox.checked) {
            allChecked = false;
          }
        });
        document.getElementById('selectAll').checked = allChecked;
      });
    });
  </script>
