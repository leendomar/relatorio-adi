<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tags') }}
        </h2>
    </x-slot>
    <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg p-6">
            <h2 class="text-lg font-semibold text-gray-900 mb-4">Editar Tag</h2>

            <form action="{{ route('tags.update', $tag->id) }}" method="POST" class="space-y-4">
                @csrf
                @method('PUT')

                <!-- Nome da Tag -->
                <div>
                    <label for="name" class="block text-sm font-medium text-gray-700">Nome da Tag</label>
                    <input type="text" id="name" name="name" value="{{ $tag->name }}" required
                           class="mt-1 block w-full rounded-lg border-gray-300 shadow-sm focus:ring-blue-500 focus:border-blue-500 p-2">
                </div>

                <!-- Nome do Cliente -->
                <div>
                    <label for="client" class="block text-sm font-medium text-gray-700">Nome do Cliente</label>
                    <input type="text" id="client" name="client" value="{{ $tag->client }}" required
                           class="mt-1 block w-full rounded-lg border-gray-300 shadow-sm focus:ring-blue-500 focus:border-blue-500 p-2">
                </div>

                <!-- Botões de Ação -->
                <div class="flex space-x-4">
                    <button type="submit"
                            class="flex items-center justify-center px-4 py-2 bg-yellow-500 text-white font-semibold rounded-lg shadow-md hover:bg-yellow-600 transition duration-200">
                        <svg class="w-5 h-5 mr-2" fill="none" stroke="currentColor" stroke-width="2" viewBox="0 0 24 24"
                             xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                  d="M4 16l6 6M20 8l-6-6m0 0L4 16m16-8L10 2"></path>
                        </svg>
                        Atualizar
                    </button>

                    <a href="{{ route('tags.index') }}"
                       class="flex items-center justify-center px-4 py-2 bg-gray-500 text-white font-semibold rounded-lg shadow-md hover:bg-gray-600 transition duration-200">
                        <svg class="w-5 h-5 mr-2" fill="none" stroke="currentColor" stroke-width="2" viewBox="0 0 24 24"
                             xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                  d="M10 19l-7-7m0 0l7-7m-7 7h18"></path>
                        </svg>
                        Voltar
                    </a>
                    
                </div>
            </form>
        </div>
    </div>
</div>

</x-app-layout>
