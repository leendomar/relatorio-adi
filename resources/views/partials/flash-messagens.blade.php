@if (session('flash_data'))
<div class="flash-messages" id="flash-messages">
    @foreach (session('flash_data') as $message)
    <div class="alert alert-{{ $message['status'] }} alert-dismissible" role="alert">
        {{ $message['text'] }}
    </div>
    @endforeach
</div>
@endif