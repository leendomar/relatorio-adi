<title>Notícias por TAG {{ $date_after }} {{ $date_before }}</title>

<table id="header">
    <tr>
        <td rowspan="4" colspan="2" id="logo">
            <img src="./images/pdf/logo.jpeg" />
        </td>
    </tr>
    <tr>
        <td>{{ Auth::user()->name }}</td>
    </tr>
    <tr>
        <td>TAG: {{ $tag }}</td>
    </tr>
    <tr>
        <td> Data: {{ $date_after }} - {{ $date_before }}</td>
    </tr>
</table>




</div>
<table id="content">

    {{ $line = null}}

    @foreach ($jornais as $jornal)
        @isset($jornal['news'])
            <tr class="table-danger" rowspan="2">
                <th colspan="3" class="jornal-name" scope="col">{{$jornal['name']}} - {{$jornal['city']}}</td>
            </tr>
            <tr>
                <th scope="col"></th>
                <th scope="col">Título</th>
                <th scope="col">Data</th>
            </tr>
            <tbody>
                @foreach ($jornal['news'] as $new)

                <tr class="{{ $loop->even ? 'even' : 'odd' }}">
                    <td>{{ ++$line  }}</td>
                    <td><a href="{{ $new['link'] }}" target="_blank"> {{ $new['title'] }}</a></td>
                    <td>{{ $new['date'] }}</td>
                </tr>

                @endforeach
            </tbody>
        @endif

    @endforeach

</table>

<style>
    table#header {
        position: relative;
        top: -40px;
    }

    table#content {
        width: 100%;
    }

    table #logo img {
        width: 250px;
    }

    table #logo {
        width: 600px;
    }

    table#header,
    table#header tr,
    table#header td {
        border: none;
    }

    tr.odd {
        background-color: #efefef;
    }

    td {
        border: 1px solid #dee2e6;
        padding: 5px 0;
    }

    .table-danger,
    .table-danger>td,
    .table-danger>th {
        background-color: #f5c6cb;
        padding: 10px;
    }
</style>
