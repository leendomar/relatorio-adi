<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prévia da Exportação</title>
    <style>
        body { font-family: Arial, sans-serif; margin: 20px; }
        table { width: 100%; border-collapse: collapse; margin-top: 10px; }
        th, td { border: 1px solid #ddd; padding: 8px; text-align: left; }
        th { background-color: #f4f4f4; }
        .journal-header { background-color: #ddd; font-weight: bold; }
    </style>
</head>
<body>
    <h2>Prévia da Exportação de Notícias</h2>

    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Jornal</th>
                <th>Título</th>
                <th>Link</th>
                <th>Data</th>
            </tr>
        </thead>
        <tbody>
            @php $index = 1; @endphp
            @foreach ($newsData as $journal => $news)
            {{ $news }}

                <tr class="journal-header">
                    <td>{{ $index++ }}</td>
                    <td colspan="4">{{ strtoupper($journal) }}</td>
                </tr>
                @foreach ($news as $newsItem)
                    <tr>
                        <td></td>
                        <td></td>
                        <td>{{ $newsItem['title'] }}</td>
                        <td><a href="{{ $newsItem['link'] }}" target="_blank">Ver Notícia</a></td>
                        <td>{{ $newsItem['date'] }}</td>
                    </tr>
                @endforeach
            @endforeach
        </tbody>
    </table>
</body>
</html>
