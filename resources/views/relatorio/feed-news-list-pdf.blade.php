<title>Notícias por TAG {{ $date_after }} {{ $date_before }}</title>

<table id="header">
    <tr>
        <td id="logo">
            <!-- Caminho da imagem absoluto (ou URL completa) -->
            <img src="{{ public_path('images/pdf/logo.jpeg') }}" />
            <div id="emp">
                <p>CNPJ: 00.038.925/0001-06</p>
                <p>END: AV Paissandu, n 525 sl 15D - Zona 03</p>
                <p>Maringá PR - CEP:87050-130</p>
                <p>Telefone: (41) 98405-2344</p>
                <p>www.adipr.com.br</p>
            </div>
        </td>
        <td>
            <div>
                <h1>Relatório de Publicações on-line</h1>
                <p>Cliente: {{ $cliente }}</p>
                <p>Período de: {{ $date_after }} a {{ $date_before }}, {{ date('H:i') }}</p>
                <p>TAG: {{ $tag }}</p>
                @if($obs)
                    <p id="obs"><b>Obs:</b> {{ $obs }}</p>
                @endif
            </div>
        </td>
    </tr>
</table>

<table id="content">
    {{ $line=1 }}
    @foreach ($feedJornais as $key_jornal => $jornal)
        @if(!empty($jornal['news']))
            <tr class="table-danger">
                <th colspan="3" class="jornal-name">{{ $jornal['name'] }} - {{ $jornal['city'] }}</th>
                @if ($view)
                <th class="v">V.</th>
                @endif          
                @if ($stay)
                <th class="p">P.</th>
                @endif
            </tr>
            <tbody>

                @foreach ($jornal['news'] as $new)
                    <tr class="{{ $loop->even ? 'even' : 'odd' }}">
                        <td class="line">{{ $line++ }}</td>
                        <td class="link"><a href="{{ $new['link'] }}" target="_blank">{{ $new['title'] }}</a></td>
                        <td class="hora">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $new['date'])->format('d/m/Y H:i') }}</td>
                        @if ($view)                            
                        <td class="v">{{ $new['view'] }}</td>
                        @endif  
                        @if ($stay)                            
                        <td class="p">{{  $new['stay']?number_format($new['stay'] / 60, 2, '.', ''):0; }}</td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        @endif
    @endforeach
</table>

<style>
    /* Estilos gerais */
    * {
        font-family: Arial, Helvetica, sans-serif;
    }


    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        border: 1px solid #dee2e6;
        padding: 8px;
        font-size: 14px;
    }

    td.line {
        text-align: center;
        width: 40px;
    }

    td.link {
        max-width: 150px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }

    th.p, th.v, td.p, td.v{
        text-align: center;
        width: 10px;
    }

    td.hora {
        text-align: center;
        width: 140px;
    }

    .jornal-name {
        font-size: 16px;
        background-color: #f5c6cb;
        text-align: center;
    }

    .even {
        background-color: #f2f2f2;
    }

    .odd {
        background-color: #ffffff;
    }

    .table-danger {
        background-color: #f8d7da;
    }

    #header {
        margin-bottom: 20px;
    }

    #heade table  th, td {
        border: 0px solid #dee2e6;

    }

    #logo img {
        width: 250px;
    }

    #emp p {
        font-size: 10px;
    }

    #obs {
        max-width: 300px;
    }

    h1 {
        font-size: 18px;
        margin-bottom: 10px;
    }

    a {
        text-decoration: none;
    }
</style>
