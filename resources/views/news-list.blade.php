<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Notícias por TAG: {{ Session('Filter.tag') }}
        </h2>
    </x-slot>


    <div class="py-12" id="content">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <form action="/news-list" method="POST" class="p-6" id="form-search">

                    @csrf
                    <!-- Your form fields here -->
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <x-input-label for="input-tag" :value="__('TAG')" />
                            <x-text-input id="input-tag" class="block mt-1 w-full" type="text" name="tag" value="{{ Session('Filter.tag') }}" />
                            <x-input-error :messages="$errors->get('tag')" class="mt-2" />
                        </div>
                        <div class="col-md-2 form-group">
                            <x-input-label for="input-date-after" :value="__('Data Inicial')" />
                            <x-text-input id="input-date-after" class="block mt-1 w-full" type="date" name="date_after" value="{{ Session('Filter.date_after') }}" />
                            <x-input-error :messages="$errors->get('date_after')" class="mt-2" />
                        </div>
                        <div class="col-md-2 form-group">
                            <x-input-label for="input-date-before" :value="__('Data Final')" />
                            <x-text-input id="input-date-before" class="block mt-1 w-full" type="date" name="date_before" value="{{ Session('Filter.date_before') }}" />
                            <x-input-error :messages="$errors->get('date_before')" class="mt-2" />
                        </div>
                        <div class="col-md-4 form-group" style="margin-top:35px;">
                            <input id="input-search" type="submit" class="items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-500 focus:outline-none transition ease-in-out duration-150" value="{{__('Search')}}">
                            <a class="items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-500 focus:outline-none transition ease-in-out duration-150" href="{{ URL::to('/news-list/clear') }}">{{__('Clear')}}</a>
                            @if(Session('haveNews'))
                            <a class="items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-500 focus:outline-none transition ease-in-out duration-150" href="{{ URL::to('/news-list/pdf') }}" target="_blank">{{__('Print')}} - PDF</a>
                            <!-- <a class="btn" href="{{ URL::to('/news-list/excel') }}" target="_blank">{{__('Planilha')}} - Excel</a> -->
                            @endisset
                        </div>
                    </div>

                </form>

                <div class="p-6 text-gray-900">
                    @include('partials.flash-messagens')

                    <div class="loader" id="loading">
                        <span class="loader-text"></span>
                    </div>



                    {{ $line = null}}

                    @if($jornais)
                    <table class="table table-bordered mb-5" id="display-infos">

                        @foreach ($jornais as $jornal)
                        @isset($jornal['news'])
                        <thead>
                            <tr class="table-danger">
                                <th colspan="3" class="jornal-name" scope="col">{{$jornal['name']}} - {{$jornal['city']}}</td>
                            </tr>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Título</th>
                                <th scope="col">Data</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($jornal['news'] as $new)
                                <tr class="{{ $loop->even ? 'even' : 'odd' }}">
                                    <td>{{ ++$line  }}</td>
                                    <td>
                                        <a href="{{ $new['link'] }}" 
                                        target="_blank"> 
                                        {{ $new['title'] }}
                                    </a>
                                    </td>
                                    <td>{{ $new['date'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        @endisset
                        @endforeach

                    </table>
                    @endif


                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<style>
    .loader {
        width: 60px;
        display: none;
    }

    .loader-wheel {
        animation: spin 1s infinite linear;
        border: 2px solid rgba(30, 30, 30, 0.5);
        border-left: 4px solid #fff;
        border-radius: 50%;
        height: 50px;
        margin-bottom: 10px;
        width: 50px;
    }

    .loader-text {
        color: #f00;
        font-family: arial, sans-serif;
    }

    .loader-text:after {
        content: 'Carregando';
        animation: load 2s linear infinite;
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }

    @keyframes load {
        0% {
            content: 'Carregando';
        }

        33% {
            content: 'Carregando.';
        }

        67% {
            content: 'Carregando..';
        }

        100% {
            content: 'Carregando...';
        }
    }
</style>
<script>
    const contentDiv = document.getElementById('content');

    document.getElementById("form-search").addEventListener("submit", function(event) {
        contentDiv.addEventListener('mouseover', function() {
            contentDiv.style.cursor = 'progress';
        });
        document.getElementById("input-search").disabled = true;
        document.getElementById("loading").style.display = "block";
        document.getElementById("display-infos").innerHTML = ''
        document.getElementById("flash-messages").innerHTML = ''
    });
</script>