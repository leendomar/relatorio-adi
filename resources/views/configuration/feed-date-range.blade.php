<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Sincronizar Views e Pernanencia
        </h2>
    </x-slot>
    <div class="py-12" id="content">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <form action="" method="POST" class="p-6" id="form-search">

                    @csrf
                    <div class="row">                     
                        <div class="col-md-2 form-group">
                            <x-input-label for="input-start_date" :value="__('Data Inicial')" />
                            <x-text-input id="input-start_date" class="block mt-1 w-full" type="date" 
                            name="start_date" value="{{ Session('Filter.feedDateRange.start_date') }}" />
                            <x-input-error :messages="$errors->get('start_date')" class="mt-2" />
                        </div>
                        <div class="col-md-2 form-group">
                            <x-input-label for="input-end_date" :value="__('Data Final')" />
                            <x-text-input id="input-end_date" class="block mt-1 w-full" type="date"
                                name="end_date" value="{{ Session('Filter.feedDateRange.end_date') }}" />
                            <x-input-error :messages="$errors->get('end_date')" class="mt-2" />
                        </div>
                        <div class="col-md-4 form-group" style="margin-top:35px;">
                            <input id="input-sync" type="submit"
                                class="items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-500 focus:outline-none transition ease-in-out duration-150"
                                value="{{__('Sync')}}">
                            <input id="input-track" type="submit"
                                class="items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-500 focus:outline-none transition ease-in-out duration-150"
                                value="{{__('Track')}}">
                        </div> 

                    <div class="col-md-12 ">
                            <label><input type="checkbox" id="showJournals" name="showJournals" {{ Session('Filter.feedDateRange.showJournals') ?'checked':''}}> Selecionar Portais</label>
                            <div class="journals-list" id="journalsList" style="display: none;">
                                <label><input type="checkbox" id="selectAll" checked> <strong>Selecionar Todos:</strong></label>
                                <div class="checkbox-container">
                                    <div class="row">
                                            @foreach ($journalList as $jornal)
                                                <div class="col-md-4">
                                                    <label>
                                                        <input type="checkbox" class="journal" name="journal_ids[]" value="{{$jornal['id']}}"
                                                            @if(Session('Filter.feedDateRange.journal_ids'))
                                                                {{in_array($jornal['id'],Session('Filter.feedDateRange.journal_ids')) ? 'checked' : '' }}
                                                            @endif
                                                        > {{ $jornal['name'] }}
                                                    </label>
                                                </div>
                                            @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                </form> 
            </div>
 
        <div class="container">
            <table id="syncHistoryTable" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>

                        <th>Início</th>
                        <th>Fim</th>
                        <th>Status</th>
                        <th>Erro</th>
                        <th class="dt-type-date">Atualizado em</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- A tabela será preenchida via AJAX -->
                </tbody>
            </table>
        </div>
    </div>
 

 
</x-app-layout> 


 

<!-- Adicionando os links do DataTables -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.7.1.js"></script>
<script src="https://cdn.datatables.net/2.2.1/js/dataTables.js"></script>
<script src="https://cdn.datatables.net/rowgroup/1.5.1/js/dataTables.rowGroup.js"></script>
<script src="https://cdn.datatables.net/rowgroup/1.5.1/js/rowGroup.dataTables.js"></script>

<script>
        $(document).ready(function() {
            // Inicializando o DataTable
            var table = $('#syncHistoryTable').DataTable({
                processing: false,
                serverSide: false,
                searching: false, 
                paging: false,      // 🔴 Desativa a paginação
                lengthChange: false, // 🔴 Remove o seletor "Entries per page"
                info: false,        // 🔴 Oculta a exibição "Mostrando X de Y registros"
                // pageLength: 32, // Define 32 linhas por página
                ajax: "{{ route('configuration.feed-date-range') }}",  // Rota que retorna os logs
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'start_date', name: 'start_date' },
                    { data: 'end_date', name: 'end_date' },
                    {
                        data: 'status',
                        render: function(data) {
                        switch(data) {
                            case 'concluído':
                                return '<span class="badge bg-success">Concluído</span>';
                            case 'erro':
                                return '<span class="badge bg-danger">Erro</span>';
                            case 'processando':
                                return '<span class="badge bg-warning">Processando</span>';
                            case 'pendente':
                                return '<span class="badge bg-info">Pendente</span>';
                            default:
                                return '<span class="badge bg-secondary">Desconhecido</span>';
                        }
                    }

                    },
                    { data: 'error_message', name: 'error_message', defaultContent: '—' },
                    { data: 'updated_at', name: 'updated_at' }
                ],
                order: [[32, 'asc']],
                rowGroup: {
                    dataSrc:  'newspaper.name'
                },
                drawCallback: function() {
                    // O que fazer quando a tabela for redesenhada
                }
            });

            // Atualiza a tabela a cada 5 segundos
            setInterval(function() {
                table.ajax.reload(null, false); // Recarrega os dados sem resetar a paginação
            }, 5000);
        });
    </script>


<script>
    // Exibir/ocultar a lista de jornais com base no checkbox "Mostrar Jornais"

    // Função que exibe ou oculta a lista de jornais com base no estado do checkbox "Mostrar Jornais"
    function toggleJournals() {
      var journalsList = document.getElementById('journalsList');
      var showJournalsCheckbox = document.getElementById('showJournals');

      // Verifica o estado do checkbox "showJournals" e altera o display da lista de jornais
      if (showJournalsCheckbox.checked) {
        journalsList.style.display = 'block';  // Exibe a lista de jornais
      } else {
        journalsList.style.display = 'none';   // Oculta a lista de jornais
      }
    }

    // Evento de mudança no checkbox "showJournals"
    document.getElementById('showJournals').addEventListener('change', toggleJournals);

    // Verificação se o checkbox "showJournals" está marcado ao carregar a página
    window.onload = function() {
      toggleJournals();  // Chama a função ao carregar a página para verificar o estado do checkbox
    };
    // Selecionar/deselecionar todos os checkboxes de jornais
    document.getElementById('selectAll').addEventListener('change', function() {
      var isChecked = this.checked;
      var journalCheckboxes = document.querySelectorAll('.journal');
      journalCheckboxes.forEach(function(checkbox) {
        checkbox.checked = isChecked;
      });
    });

    // Sincronizar o "Selecionar Todos" com os outros checkboxes
    document.querySelectorAll('.journal').forEach(function(checkbox) {
      checkbox.addEventListener('change', function() {
        var allChecked = true;
        var journalCheckboxes = document.querySelectorAll('.journal');
        journalCheckboxes.forEach(function(checkbox) {
          if (!checkbox.checked) {
            allChecked = false;
          }
        });
        document.getElementById('selectAll').checked = allChecked;
      });
    });
  </script>
  
<script>
    $(document).ready(function () {
        $("#input-sync, #input-track").click(function (event) {
            event.preventDefault();

            const button = $(this); // Captura o botão clicado
            const originalText = button.val(); // Armazena o texto original do botão

            // Desativa o botão e exibe o spinner de loading
            button.prop("disabled", true).val("Processando...");
            button.addClass("loading");

            const form = document.getElementById("form-search");
            const formData = new FormData(form);

            const actionUrl = button.attr("id") === "input-sync"
                ? "{{ route('configuration.store-feed-date-range') }}"
                : "{{ route('configuration.track-feed-date-range') }}";

            $.ajax({
                url: actionUrl,
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                headers: {
                    "X-CSRF-TOKEN": $('input[name="_token"]').val()
                },
                success: function (response) {
                    console.log("Sucesso:", response);
                    // Atualiza a DataTable (caso exista)
                    if ($.fn.DataTable.isDataTable("#syncHistoryTable")) {
                        $("#syncHistoryTable").DataTable().ajax.reload(null, false);
                    }
                },
                error: function (xhr) {
                    console.error("Erro:", xhr.responseText);
                    alert("Erro ao processar a solicitação.");
                },
                complete: function () {
                    // Reabilita o botão e restaura o texto original
                    button.prop("disabled", false).val(originalText);
                    button.removeClass("loading");
                }
            });
        });
    });
</script>


<style>
td.dt-type-date {
    white-space: nowrap;
}
th.dt-type-date{
    width: 9em;
}
</style>