<x-app-layout>
    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')" />

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div class="p-6 text-gray-900">
                    <div class="block font-medium text-sm text-gray-700">

                        <form method="POST" action="{{ route('plugin.upload') }}" enctype="multipart/form-data" autocomplete="off">
                            @csrf

                            <!-- Arquivo do Plugin -->
                            <div class="mt-6 col-5">
                                <x-input-label for="plugin" :value="__('Arquivo do Plugin (ZIP)')" />
                                <x-text-input id="plugin" class="block mt-1 w-full" type="file" name="plugin" required />
                                <x-input-error :messages="$errors->get('plugin')" class="mt-2" />
                            </div>

                            <!-- Nome -->
                            <div class="mt-6 col-5">
                                <x-input-label for="name" :value="__('Nome do Plugin')" />
                                <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" value="{{ old('name', $defaults['name']) }}" required />
                                <x-input-error :messages="$errors->get('name')" class="mt-2" />
                            </div>

                            <!-- Slug -->
                            <div class="mt-6 col-5">
                                <x-input-label for="slug" :value="__('Slug')" />
                                <x-text-input id="slug" class="block mt-1 w-full" type="text" name="slug" value="{{ old('slug', $defaults['slug']) }}" required />
                                <x-input-error :messages="$errors->get('slug')" class="mt-2" />
                            </div>

                            <!-- Versão -->
                            <div class="mt-6 col-5">
                                <x-input-label for="version" :value="__('Versão')" />
                                <x-text-input id="version" class="block mt-1 w-full" type="text" name="version" value="{{ old('version', $defaults['version']) }}" required />
                                <x-input-error :messages="$errors->get('version')" class="mt-2" />
                            </div>

                            <!-- Requer -->
                            <div class="mt-6 col-5">
                                <x-input-label for="requires" :value="__('Requer')" />
                                <x-text-input id="requires" class="block mt-1 w-full" type="text" name="requires" value="{{ old('requires', $defaults['requires']) }}" required />
                                <x-input-error :messages="$errors->get('requires')" class="mt-2" />
                            </div>

                            <!-- Testado com -->
                            <div class="mt-6 col-5">
                                <x-input-label for="tested" :value="__('Testado com')" />
                                <x-text-input id="tested" class="block mt-1 w-full" type="text" name="tested" value="{{ old('tested', $defaults['tested']) }}" required />
                                <x-input-error :messages="$errors->get('tested')" class="mt-2" />
                            </div>

                            <!-- Autor -->
                            <div class="mt-6 col-5">
                                <x-input-label for="author" :value="__('Autor')" />
                                <x-text-input id="author" class="block mt-1 w-full" type="text" name="author" value="{{ old('author', $defaults['author']) }}" required />
                                <x-input-error :messages="$errors->get('author')" class="mt-2" />
                            </div>

                            <!-- Homepage -->
                            <div class="mt-6 col-5">
                                <x-input-label for="homepage" :value="__('Homepage')" />
                                <x-text-input id="homepage" class="block mt-1 w-full" type="url" name="homepage" value="{{ old('homepage', $defaults['homepage']) }}" required />
                                <x-input-error :messages="$errors->get('homepage')" class="mt-2" />
                            </div>

                            <!-- Aviso de Atualização -->
                            <div class="mt-6 col-5">
                                <x-input-label for="upgrade_notice" :value="__('Aviso de Atualização')" />
                                <x-textarea id="upgrade_notice" class="block mt-1 w-full" name="upgrade_notice">{{ old('upgrade_notice', $defaults['upgrade_notice']) }}</x-textarea>
                                <x-input-error :messages="$errors->get('upgrade_notice')" class="mt-2" />
                            </div>

                            <!-- Botão de Enviar -->
                            <div class="flex items-center justify-end mt-4 col-5">
                                <x-primary-button class="ms-3">
                                    {{ __('Fazer Upload') }}
                                </x-primary-button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
