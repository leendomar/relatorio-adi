<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Newspapers') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 p-6">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <!-- Botão Criar Portal com destaque -->
                    <a class="inline-flex items-center px-4 py-2 bg-blue-600 text-sm font-medium rounded-md shadow-md hover:bg-blue-700 focus:outline-none transition duration-200 mb-4" 
   href="{{ URL::to('/newspapers/create') }}">
    {{__('Adicionar um Portal')}}
</a>

                    <table class="table table-bordered mb-5">

                        {{ $line = null}}

                        <thead>
                            <tr>
                                <th scope="col">N</th>
                                <th scope="col">Cidade</th>
                                <th scope="col">Jornal</th>
                                <th scope="col">Portal</th>
                                <!-- <th scope="col">Tipo de Csonteúdo</th> -->
                                <th scope="col">WP</th>
                                <th scope="col">P.Versão</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($newspapers as $newspaper)

                            <tr class="{{ $loop->even ? 'bg-gray-50' : 'bg-white' }}">
                                <td>{{ $newspaper->id }}</td>
                                <td>{{ $newspaper->city }}</td>
                                <td>{{ $newspaper->name }}</td>
                                <td><a target="_blank" href="{{ $newspaper->url }}">{{ $newspaper->url }}</a></td>
                                <!-- <td>{{ $newspaper->type_content }}</td> -->
                                <td> 
                                    @if($newspaper->wp_active)
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="currentColor">
                                        <path fill="#ceb522" d="M12 2C6.5 2 2 6.5 2 12s4.5 10 10 10s10-4.5 10-10S17.5 2 12 2m-2 15l-5-5l1.41-1.41L10 14.17l7.59-7.59L19 8z"/>
                                    </svg>
                                    @endif
                                </td>
                                <td>{{$newspaper->wp_version}}</td>
                               
                                
                                <td class="flex items-center space-x-2">
    <!-- Botão de Editar -->
    <a class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-700 focus:outline-none transition ease-in-out duration-150" 
       href="/newspapers/{{$newspaper->id}}/edit">
        {{__('Editar')}}
    </a>

    <!-- Botão de Ativar/Desativar -->
    <form action="{{ route('newspapers.destroy',$newspaper->id) }}" method="POST" class="inline">
        @csrf
        @method('DELETE')
        <button type="submit" 
            class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-white {{ $newspaper->status == 'inativo' ? 'bg-green-500 hover:bg-green-600' : 'bg-red-500 hover:bg-red-600' }} focus:outline-none transition ease-in-out duration-150">
            @if($newspaper->status == 'inativo')
                Ativar
            @else
                Desativar
            @endif
        </button>
    </form>
</td>

                            </tr>

                            @endforeach
                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
