<x-app-layout>
    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')" />

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                @if (Session::has('success'))
                    <div class="max-w-7xl mx-auto  ">
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    </div>
                @endif
                <div class="p-6 text-gray-900">
    <div class="block font-medium text-sm text-gray-700">
        <form method="POST" action="{{ route('newspapers.update', $newspaper->id) }}" class="grid grid-cols-1 sm:grid-cols-2 gap-6" autocomplete="off">
            @csrf
            @method('PUT')

            <!-- City -->
            <div class="input-group">
                <x-input-label for="city" :value="__('Cidade')" />
                <x-text-input id="city" class="block mt-1 w-full" type="text" name="city" value="{{$newspaper->city}}" />
                <x-input-error :messages="$errors->get('city')" class="mt-2" />
            </div>

            <!-- Name -->
            <div class="input-group">
                <x-input-label for="name" :value="__('Jornal')" />
                <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" value="{{$newspaper->name}}" />
                <x-input-error :messages="$errors->get('name')" class="mt-2" />
            </div>

            <!-- URL -->
            <div class="input-group">
                <x-input-label for="url" :value="__('URL')" />
                <x-text-input id="url" class="block mt-1 w-full" type="text" name="url" value="{{$newspaper->url}}" />
                <x-input-error :messages="$errors->get('url')" class="mt-2" />
            </div>

            <!-- Type of Content -->
            <div class="input-group">
                <x-input-label for="type_content" :value="__('Tipo de Conteúdo')" />
                <x-text-input id="type_content" class="block mt-1 w-full" type="text" name="type_content" value="{{$newspaper->type_content}}" />
                <x-input-error :messages="$errors->get('type_content')" class="mt-2" />
            </div>

            <!-- Save Button -->
            <div class="flex items-center justify-end mt-4 col-span-2">
                <x-primary-button class="ms-3">
                    {{ __('Salvar') }}
                </x-primary-button>
            </div>
        </form>
    </div>
</div>

            </div>
        </div>
    </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div class=" block font-medium text-sm text-gray-700 ">

                        <section class="space-y-6 ">
                            <header>
                                <h2 class="text-lg font-medium text-gray-900">
                                    FEEDs
                                </h2>
                                <p class="mt-1 text-sm text-gray-600">
                                    Lista de TAGS e Caminho do Feed (URL) vinculadas ao jornal
                                </p>
                            </header>

                            <div class="flex items-center justify-end mt-4  col-12">

                                <form id="add-tag-path-form">
                                    <!-- <x-text-input id="add-tag" name="tag" type="text" placeholder="{{ __('TAG') }}" autocomplete="off" /> -->


                                        <select name="tag" id="add-tag" class="block mt-1 w-full  select2"   autocomplete="off" >
                                            <option value="" disabled selected>Selecione uma Tag</option>
                                            @foreach($tagsList as $tag)
                                                <option value="{{ $tag['name'] }}">{{ $tag['name'] }} - {{ $tag['client'] }}</option>
                                            @endforeach
                                        </select>

                                    <x-text-input id="add-path" name="path" type="text" placeholder="{{ __('Path') }}"  autocomplete="off" />

                                    <x-primary-button size="small m-1">Adicionar TAG</x-primary-button>


                                    <x-input-error :messages="$errors->get('tag')" />
                                    <x-input-error :messages="$errors->get('path')" />

                                </form>
                            </div>



                        </section>
                        <x-bladewind::table compact="true" divider="thin" exclude_columns="id, marital_status"
                            data="{{ json_encode($newspaper['tagurl']) }}" :action_icons="$action_icons"
                            no_data_message=""></x-bladewind::table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

    <script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}'
            }
        });


        $('#add-tag-path-form').submit(function (event) {
            event.preventDefault();

            $.ajax({
                type: 'POST',
                url: '{{ route('newspaper-tagurl.store') }}',
                data: {
                    "newspaper_id": "{{ $newspaper->id }}",
                    "tag": $('#add-tag').val(),
                    "path": $('#add-path').val(),
                },
                dataType: 'json',
                success: function (response) {
                    $('#add-tag').val('')
                    $('#add-path').val('')
                    if (response.success) {
                        let data = response.data

                        let deleteHtml = `<td  class="text-right space-x-2 actions">
                            <button class="bw-button-circle tiny primary  !bg-red-500 hover:!bg-red-600 focus:ring-red-500 active:bg-red-600 focus:ring cursor-pointer rounded-full   has-icon" title = "" onclick = "deleteTagurl(${data.id})" href = "" type = "button" >
                            <svg class="size-6 inline-block stroke-2 !size-[18px]" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"></path>
                            </svg><span class="grow "></span>
                                </button ></td>`;

                        $('.bw-table tbody').append(`<tr data-id="${data.id}"><td>${data.tag}</td><td>${data.path}</td>${deleteHtml}</tr>`);

                    } else {

                        alert(response.error);
                    }
                },
                error: function (xhr, status, error) {
                    // Handle the error
                    alert('Error: ' + error);
                }
            });
        });

        function deleteTagurl(id) {

            $.ajax({
                type: 'DELETE',
                url: '/newspaper-tagurl/destroy/' + id,
                data: { _token: '{{ csrf_token() }}' },
                success: function (response) {
                    if (response.success) {

                        $(`[data-id='${id}']`).remove()

                    } else {
                        alert(response.error);
                    }
                },
                error: function (xhr, status, error) {
                    // Handle the error
                    alert('Error: ' + error);
                }
            });

        }


    </script>


</x-app-layout>


<!-- Adicionar CSS do Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />

<!-- Adicionar JS do Select2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js"></script>
<script>
    $(document).ready(function () {
        $('#add-tag').select2({
            theme: 'bootstrap-5',
            placeholder: "Selecione ou digite uma Tag",
            allowClear: true // Adiciona opção de limpar o campo
        });
    });
</script>

