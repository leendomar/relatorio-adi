<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        Upload de Plugin WordPress
    </h2>
    </x-slot>
    <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg p-6">
           
        <form action="{{ url('/plugin/upload') }}" method="POST" enctype="multipart/form-data" class="mb">
    @csrf

    <!-- Arquivo do Plugin -->
    <div class="mb-3">
        <label for="plugin" class="form-label">Arquivo do Plugin (ZIP)</label>
        <input type="file" name="plugin" class="form-control" id="plugin" required>
    </div>

    <!-- Nome do Plugin -->
    <div class="mb-3">
        <label for="name" class="form-label">Nome do Plugin</label>
        <input type="text" name="name" class="form-control" id="name" value="{{ old('name', $defaults['name']) }}" required>
    </div>

    <!-- Slug -->
    <div class="mb-3">
        <label for="slug" class="form-label">Slug</label>
        <input type="text" name="slug" class="form-control" id="slug" value="{{ old('slug', $defaults['slug']) }}" required>
    </div>

    <!-- Versão -->
    <div class="mb-3">
        <label for="version" class="form-label">Versão</label>
        <input type="text" name="version" class="form-control" id="version" value="{{ old('version', $defaults['version']) }}" required>
    </div>

    <!-- Requer -->
    <div class="mb-3">
        <label for="requires" class="form-label">Requer</label>
        <input type="text" name="requires" class="form-control" id="requires" value="{{ old('requires', $defaults['requires']) }}" required>
    </div>

    <!-- Testado com -->
    <div class="mb-3">
        <label for="tested" class="form-label">Testado com</label>
        <input type="text" name="tested" class="form-control" id="tested" value="{{ old('tested', $defaults['tested']) }}" required>
    </div>

    <!-- Autor -->
    <div class="mb-3">
        <label for="author" class="form-label">Autor</label>
        <input type="text" name="author" class="form-control" id="author" value="{{ old('author', $defaults['author']) }}" required>
    </div>

    <!-- Homepage -->
    <div class="mb-3">
        <label for="homepage" class="form-label">Homepage</label>
        <input type="url" name="homepage" class="form-control" id="homepage" value="{{ old('homepage', $defaults['homepage']) }}" required>
    </div>

    <!-- Aviso de Atualização -->
    <div class="mb-3">
        <label for="upgrade_notice" class="form-label">Aviso de Atualização</label>
        <textarea name="upgrade_notice" class="form-control" id="upgrade_notice">{{ old('upgrade_notice', $defaults['upgrade_notice']) }}</textarea>
    </div>

    <button type="submit" class="btn btn-warning">Fazer Upload</button>
</form>

        </div>
    </div>
</div>

</x-app-layout>
