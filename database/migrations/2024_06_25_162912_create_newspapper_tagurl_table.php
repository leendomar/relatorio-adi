<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('newspaper_tagurls', function (Blueprint $table) {
            $table->id();
            $table->foreignId('newspaper_id');
            $table->string('tag');
            $table->string('url');
            $table->foreign('newspaper_id')
            ->references('id')->on('newspapers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('newspapper_tagurls');
    }
};
