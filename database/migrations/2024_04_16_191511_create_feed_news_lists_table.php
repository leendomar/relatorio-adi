<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('feed_news_lists', function (Blueprint $table) {
            $table->id();
            $table->foreignId('newspaper_id');
            $table->text('title');
            $table->text('link');
            $table->string('tag');
            $table->dateTime('date');
            $table->foreign('newspaper_id')
            ->references('id')
            ->on('newspapers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('feed_news_lists');
    }
};
