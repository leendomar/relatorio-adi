<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    public function run(): void
    {
        $tags = [
            [
                'name' => 'govbr',
                'client' => 'Governo Federal',
                'is_active' => true,
            ],
            [
                'name' => 'govpr',
                'client' => 'Governo do Estado',
                'is_active' => true,
            ],
            [
                'name' => 'alep',
                'client' => 'Assembleia Legislativa',
                'is_active' => true,
            ],
            [
                'name' => 'itaipu',
                'client' => 'Itaipu Binacional',
                'is_active' => true,
            ],
        ];

        foreach ($tags as $tag) {
            Tag::updateOrCreate(['name' => $tag['name']], $tag);
        }
    }
}
